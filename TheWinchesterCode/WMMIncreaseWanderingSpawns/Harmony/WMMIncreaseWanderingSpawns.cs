﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using UnityEngine;

public class WMMIncreaseWanderingSpawns
{
    public class WMMIncreaseWanderingSpawns_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(AIWanderingHordeSpawner), MethodType.Constructor)]
    [HarmonyPatch("AIWanderingHordeSpawner")]
    [HarmonyPatch(new Type[] { typeof(World), typeof(AIWanderingHordeSpawner.HordeArrivedDelegate), typeof(List<AIDirectorPlayerState>), typeof(int), typeof(ulong), typeof(Vector3), typeof(Vector3), typeof(Vector3), typeof(int), typeof(bool), })]
    static class PatchAIWanderingHordeSpawner
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            var codes = new List<CodeInstruction>(instructions);

            for (int i = 0; i < codes.Count; i++)
            {
                if (codes[i].opcode == OpCodes.Ldc_I4_S)
                {
                    codes[i].opcode = OpCodes.Ldc_I4;
                    codes[i].operand = (int)10000;
                    break;
                }
            }
            return codes.AsEnumerable();
        }
    }
}

