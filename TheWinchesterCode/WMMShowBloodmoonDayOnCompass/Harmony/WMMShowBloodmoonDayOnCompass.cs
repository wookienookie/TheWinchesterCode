﻿using System;
using HarmonyLib;
using System.Reflection;
using UnityEngine;

public class WMMShowBloodmoonDayOnCompass
{
    public class WMMShowBloodmoonDayOnCompass_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_CompassWindow))]
    [HarmonyPatch("GetBindingValue")]
    [HarmonyPatch(new Type[] { typeof(string), typeof(string) }, new ArgumentType[] { ArgumentType.Ref, ArgumentType.Normal })]
    class PatchXUiC_CompassWindowGetBindingValue
    {
        static void Postfix(XUiC_ItemInfoWindow __instance, ref bool __result, ref string value, ref string bindingName)
        {
            var bloodmoon = GameStats.GetInt(EnumGameStats.BloodMoonDay);

            if(bloodmoon == 0)
            {
                return;
            }

            if (bindingName == "bloodmoontitle")
            {
                value = Localization.Get("bloodmoontitle");
            }
            else if (bindingName == "bloodmoonday")
            {
                value = GameStats.GetInt(EnumGameStats.BloodMoonDay).ToString();
            }
        }
    }
}
