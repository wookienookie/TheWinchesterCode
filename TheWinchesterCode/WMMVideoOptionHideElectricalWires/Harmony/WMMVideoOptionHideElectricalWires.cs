﻿using HarmonyLib;
using System;
using System.Reflection;
using UnityEngine;

public class WMMVideoOptionHideElectricalWires
{
    //private const float ElectricWireRadius = 0.01f;

    //public class WMMVideoOptionHideElectricalWires_Init : IModApi
    //{
    //    public void InitMod(Mod _modInstance)
    //    {
    //        Log.Out(" Loading Patch: " + this.GetType().ToString());

    //        // Reduce extra logging stuff
    //        Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
    //        Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

    //        var harmony = new HarmonyLib.Harmony(GetType().ToString());
    //        harmony.PatchAll(Assembly.GetExecutingAssembly());
    //    }
    //}

    //[HarmonyPatch(typeof(ItemActionConnectPower))]
    //[HarmonyPatch("StartHolding")]
    //[HarmonyPatch(new Type[] { typeof(ItemActionData) })]
    //public class PatchItemActionConnectPowerStartHolding
    //{
    //    static void Postfix(ItemActionConnectPower __instance)
    //    {
    //        for (int i = 0; i < WireManager.Instance.activeWires.Count; i++)
    //        {
    //            var wire = WireManager.Instance.wireList[i];
    //            var wireRadius = AccessTools.Field(typeof(FastWireNode), "wireRadius").GetValue(wire) as float?;
    //            if (wireRadius.HasValue && wireRadius.Value == 0)
    //            {
    //                wire.SetWireRadius(ElectricWireRadius);
    //                wire.BuildMesh();
    //            }
    //        }
    //    }
    //}

    //[HarmonyPatch(typeof(ItemActionConnectPower))]
    //[HarmonyPatch("StopHolding")]
    //[HarmonyPatch(new Type[] { typeof(ItemActionData) })]
    //public class PatchItemActionConnectPowerStopHolding
    //{
    //    static void Postfix(ItemActionConnectPower __instance)
    //    {
    //        for (int i = 0; i < WireManager.Instance.wireList.Count; i++)
    //        {
    //            var wire = WireManager.Instance.wireList[i];
    //            var wireRadius = AccessTools.Field(typeof(FastWireNode), "wireRadius").GetValue(wire) as float?;
    //            if (wireRadius.HasValue && wireRadius.Value == ElectricWireRadius)
    //            {
    //                wire.SetWireRadius(0);
    //                wire.BuildMesh();
    //            }
    //        }
    //    }
    //}

    //[HarmonyPatch(typeof(XUiC_Toolbelt))]
    //[HarmonyPatch("Update")]
    //[HarmonyPatch(new Type[] { typeof(float) })]
    //public class PatchXUiC_ToolbeltUpdate
    //{
    //    static void Postfix(XUiC_Toolbelt __instance)
    //    {
    //        if (!__instance.CustomAttributes.GetBool("IsWiresLoaded"))
    //        {
    //            var newWireRadius = 0f;

    //            if (__instance.xui.PlayerInventory.Toolbelt.holdingItem != null && __instance.xui.PlayerInventory.Toolbelt.holdingItem.GetItemName() == "meleeToolWireTool")
    //            {
    //                newWireRadius = ElectricWireRadius;
    //            }

    //            for (int i = 0; i < WireManager.Instance.wireList.Count; i++)
    //            {
    //                var wire = WireManager.Instance.wireList[i];
    //                var wireRadius = AccessTools.Field(typeof(FastWireNode), "wireRadius").GetValue(wire) as float?;
    //                if (wireRadius.HasValue && wireRadius.Value == ElectricWireRadius)
    //                {
    //                    wire.SetWireRadius(newWireRadius);
    //                    wire.BuildMesh();
    //                }
    //            }

    //            __instance.CustomAttributes.SetBool("IsWiresLoaded", true);
    //        }
    //    }
    //}
}