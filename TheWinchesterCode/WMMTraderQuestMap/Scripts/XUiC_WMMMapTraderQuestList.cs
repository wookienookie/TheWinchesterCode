﻿using System.Linq;
using UnityEngine;

public class XUiC_WMMMapTraderQuestList : XUiController
{
	public XUiC_Paging pager;

	public int currentPage;

	private bool updateWaypointsNextUpdate;

	private int cCountWaypointsPerPage = 5;

	private XUiC_WMMMapWaypointListEntry SelectedWaypointEntry;

	private Waypoint SelectedWaypoint;

	private XUiC_WMMMapTraderQuests mapTraderQuestsWindow;

	private XUiC_WMMMapArea mapArea;

	public override void Init()
	{
		base.Init();

		pager = base.Parent.GetChildByType<XUiC_Paging>();
		if (pager != null)
		{
			pager.OnPageChanged += delegate ()
			{
				currentPage = pager.CurrentPageNumber;
				UpdateWaypointsList(false);
			};
		}
		mapTraderQuestsWindow = (XUiC_WMMMapTraderQuests)xui.GetWindow("mapTraderQuests").Controller;
		mapArea = (XUiC_WMMMapArea)xui.GetWindow("mapAreaTrader").Controller;
	}

	public override void Update(float _dt)
	{
		base.Update(_dt);
		if (updateWaypointsNextUpdate)
		{
			updateWaypointsNextUpdate = false;
			UpdateWaypointsList(true);
		}
	}

	public override void OnOpen()
	{
		base.OnOpen();

		UpdateWaypointsList(true);
	}
    public override void OnClose()
    {
        base.OnClose();

		SelectedWaypointEntry = null;
		SelectedWaypoint = null;
	}

    public void UpdateWaypointsList(bool initialSetup, Waypoint _selectThisWaypoint = null)
	{
		if (pager == null)
		{
			updateWaypointsNextUpdate = true;
			return;
		}

		for (int i = 0; i < cCountWaypointsPerPage; i++)
		{
			XUiC_WMMMapWaypointListEntry xuiC_MapWaypointListEntry = (XUiC_WMMMapWaypointListEntry)children[i];
			if (xuiC_MapWaypointListEntry != null)
			{
				xuiC_MapWaypointListEntry.Index = i;
				xuiC_MapWaypointListEntry.Sprite.SpriteName = string.Empty;
				xuiC_MapWaypointListEntry.Name.Text = string.Empty;
				xuiC_MapWaypointListEntry.Distance.Text = string.Empty;
				xuiC_MapWaypointListEntry.Waypoint = null;
				xuiC_MapWaypointListEntry.Selected = false;
				xuiC_MapWaypointListEntry.Background.Enabled = false;
			}
		}

		var entityTrader = xui.Dialog.Respondent as EntityTrader;
		var entityPlayer = xui.playerUI.entityPlayer;
		var playerFactionTier = entityPlayer.QuestJournal.GetCurrentFactionTier(entityTrader.NPCInfo.QuestFaction, 0, false);

		if(initialSetup)
        {
			currentPage = playerFactionTier - 1;
		}

		if(_selectThisWaypoint != null)
        {
			SelectedWaypoint = _selectThisWaypoint;
		}

		if (pager != null)
		{
			pager.SetLastPageByElementsAndPageLength(playerFactionTier * cCountWaypointsPerPage, cCountWaypointsPerPage);
			pager.SetPage(currentPage);
		}

		var waypoints = entityPlayer.Waypoints.List
			.Where(x => x.navObject.HiddenDisplayName == WMMConstants.HiddenDisplayName
			&& x.navObject.ExtraData == currentPage + 1)	
			.Take(5)
			.ToList();

		for (int i = 0; i < waypoints.Count; i++)
		{
			XUiC_WMMMapWaypointListEntry xuiC_MapWaypointListEntry = (XUiC_WMMMapWaypointListEntry)children[i];
			if (xuiC_MapWaypointListEntry != null)
			{
				var waypoint = waypoints[i];
				xuiC_MapWaypointListEntry.Background.Enabled = true;
				xuiC_MapWaypointListEntry.Index = i;
				xuiC_MapWaypointListEntry.Sprite.SpriteName = waypoint.icon;
				xuiC_MapWaypointListEntry.Name.Text = Localization.Get(waypoint.DisplayName);
				xuiC_MapWaypointListEntry.Waypoint = waypoint;
				xuiC_MapWaypointListEntry.Selected = SelectedWaypoint != null && SelectedWaypoint.Equals(waypoint);

				Vector3 a = waypoint.pos.ToVector3();
				Vector3 position = entityPlayer.GetPosition();
				a.y = 0f;
				position.y = 0f;
				float num3 = (a - position).magnitude;
				string arg = "m";
				if (num3 >= 1000f)
				{
					num3 /= 1000f;
					arg = "km";
				}
				var direction = ValueDisplayFormatters.Direction(GameUtils.GetDirByNormal(new Vector2(waypoint.pos.x - position.x, waypoint.pos.z - position.z)), false);
				xuiC_MapWaypointListEntry.Distance.Text = string.Format("{0} {1} {2}", num3.ToCultureInvariantString("0.0"), arg, direction);
				
				if (SelectedWaypointEntry == null)
				{
					SelectedWaypointEntry = xuiC_MapWaypointListEntry;
					var quest = WMMTraderQuestMapCore.GetQuestFromPosition(waypoint.pos);
					if (quest != null)
					{
						xuiC_MapWaypointListEntry.Selected = true;
						SelectedWaypoint = waypoint;
						mapTraderQuestsWindow.SelectQuestWaypoint(quest, waypoint.entityId);
						mapArea.PositionMapAt(waypoint.pos.ToVector3());
					}
				}
			}
		}
	}
}
