﻿using HarmonyLib;

public class XUiC_WMMDialogWindowGroup : XUiC_DialogWindowGroup
{
    public XUiC_WMMMapArea mapArea;
    public XUiC_WMMMapTraderQuests traderQuests;

    public override void OnOpen()
    {
        base.OnOpen();

        //Log.Out("XUiC_WMMDialogWindowGroup OPENED ");

        mapArea.ViewComponent.IsVisible = false;
        traderQuests.ViewComponent.IsVisible = false;
    }

    public override void Init()
    {
        base.Init();

        var _mapArea = base.GetChildByType<XUiC_WMMMapArea>();
        if (_mapArea != null)
        {
            mapArea = _mapArea;
        }

        var _traderQuests = base.GetChildByType<XUiC_WMMMapTraderQuests>();
        if (_traderQuests != null)
        {
            traderQuests = _traderQuests;
        }
    }
}