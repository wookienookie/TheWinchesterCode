﻿using HarmonyLib;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class XUiC_WMMMapArea : XUiC_MapArea
{
    private XUiV_Button btnTier1;

    private XUiV_Button btnTier2;

    private XUiV_Button btnTier3;

    private XUiV_Button btnTier4;

    private XUiV_Button btnTier5;

    private XUiV_Button btnTier6;

    private XUiV_Button btnTier7;

    private XUiV_Button btnTier8;

    private XUiV_Button btnTier9;

    private XUiV_Button btnTier10;

    private XUiController tier1panel;

    private XUiController tier2panel;

    private XUiController tier3panel;

    private XUiController tier4panel;

    private XUiController tier5panel;

    private XUiController tier6panel;

    private XUiController tier7panel;

    private XUiController tier8panel;

    private XUiController tier9panel;

    private XUiController tier10panel;

    private XUiController waypointIcon;

    private XUiController waypointDistance;

    private Dictionary<int, bool> buttonToggles = new Dictionary<int,bool>();

    public override void Init()
    {
        base.Init();

        tier1panel = GetChildById("tier1panel");
        tier2panel = GetChildById("tier2panel");
        tier3panel = GetChildById("tier3panel");
        tier4panel = GetChildById("tier4panel");
        tier5panel = GetChildById("tier5panel");
        tier6panel = GetChildById("tier6panel");
        tier7panel = GetChildById("tier7panel");
        tier8panel = GetChildById("tier8panel");
        tier9panel = GetChildById("tier9panel");
        tier10panel = GetChildById("tier10panel");

        btnTier1 = (XUiV_Button)GetChildById("tier1Button").ViewComponent;
        btnTier1.Controller.OnPress += delegate (XUiController _sender, int _args)
        {
            btnTier1.Selected = !btnTier1.Selected;
            ToggleNavObjectVisibility(1);
            SaveButtonToggle(1, btnTier1.Selected);
        };

        btnTier2 = (XUiV_Button)GetChildById("tier2Button").ViewComponent;
        btnTier2.Controller.OnPress += delegate (XUiController _sender, int _args)
        {
            btnTier2.Selected = !btnTier2.Selected;
            ToggleNavObjectVisibility(2);
            SaveButtonToggle(2, btnTier2.Selected);
        };

        btnTier3 = (XUiV_Button)GetChildById("tier3Button").ViewComponent;
        btnTier3.Controller.OnPress += delegate (XUiController _sender, int _args)
        {
            btnTier3.Selected = !btnTier3.Selected;
            ToggleNavObjectVisibility(3);
            SaveButtonToggle(3, btnTier3.Selected);
        };

        btnTier4 = (XUiV_Button)GetChildById("tier4Button").ViewComponent;
        btnTier4.Controller.OnPress += delegate (XUiController _sender, int _args)
        {
            btnTier4.Selected = !btnTier4.Selected;
            ToggleNavObjectVisibility(4);
            SaveButtonToggle(4, btnTier4.Selected);
        };

        btnTier5 = (XUiV_Button)GetChildById("tier5Button").ViewComponent;
        btnTier5.Controller.OnPress += delegate (XUiController _sender, int _args)
        {
            btnTier5.Selected = !btnTier5.Selected;
            ToggleNavObjectVisibility(5);
            SaveButtonToggle(5, btnTier5.Selected);
        };

        btnTier6 = (XUiV_Button)GetChildById("tier6Button").ViewComponent;
        btnTier6.Controller.OnPress += delegate (XUiController _sender, int _args)
        {
            btnTier6.Selected = !btnTier6.Selected;
            ToggleNavObjectVisibility(6);
            SaveButtonToggle(6, btnTier6.Selected);
        };

        btnTier7 = (XUiV_Button)GetChildById("tier7Button").ViewComponent;
        btnTier7.Controller.OnPress += delegate (XUiController _sender, int _args)
        {
            btnTier7.Selected = !btnTier7.Selected;
            ToggleNavObjectVisibility(7);
            SaveButtonToggle(7, btnTier7.Selected);
        };

        btnTier8 = (XUiV_Button)GetChildById("tier8Button").ViewComponent;
        btnTier8.Controller.OnPress += delegate (XUiController _sender, int _args)
        {
            btnTier8.Selected = !btnTier8.Selected;
            ToggleNavObjectVisibility(8);
            SaveButtonToggle(8, btnTier8.Selected);
        };

        btnTier9 = (XUiV_Button)GetChildById("tier9Button").ViewComponent;
        btnTier9.Controller.OnPress += delegate (XUiController _sender, int _args)
        {
            btnTier9.Selected = !btnTier9.Selected;
            ToggleNavObjectVisibility(9);
            SaveButtonToggle(9, btnTier9.Selected);
        };

        btnTier10 = (XUiV_Button)GetChildById("tier10Button").ViewComponent;
        btnTier10.Controller.OnPress += delegate (XUiController _sender, int _args)
        {
            btnTier10.Selected = !btnTier10.Selected;
            ToggleNavObjectVisibility(10);
            SaveButtonToggle(10, btnTier10.Selected);
        };

        waypointIcon = GetChildById("waypointIcon");
        waypointDistance = GetChildById("waypointDistance");
    }

    private void ToggleNavObjectVisibility(int difficultyTier)
    {
        // So this hack to toggle the visiblity of the map NavObjects....
        // The code will automatically show/hide pins on the map by property bool on navObject.HasRequirements but cannot access that!
        // So we use a property called navObject.hasOnScreen which we can access and then use that in a Harmony Patch on the navObject.HasRequirements Getter! YUK

        List<NavObject> navObjectList = NavObjectManager.Instance.NavObjectList;
        for (int i = 0; i < navObjectList.Count; i++)
        {
            NavObject navObject = navObjectList[i];

            if (navObject.HiddenDisplayName == WMMConstants.HiddenDisplayName
                && navObject.ExtraData == difficultyTier)
            {
                var hasOnScreen = AccessTools.Field(typeof(NavObject), "hasOnScreen").GetValue(navObject) as bool?;

                if (hasOnScreen.HasValue)
                {
                    AccessTools.Field(typeof(NavObject), "hasOnScreen").SetValue(navObject, !hasOnScreen.Value);
                }
            }
        }
    }

    public override void OnOpen()
    {
        base.OnOpen();

        waypointIcon.ViewComponent.IsVisible = false;
        waypointDistance.ViewComponent.IsVisible = false;

        var entityPlayer = xui.playerUI.entityPlayer;
        var uiforPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayer);
        var entityTrader = uiforPlayer.xui.Dialog.Respondent as EntityTrader;

        if (entityTrader != null)
        {
            // see DialogResponseQuest
            List<Quest> activeQuests = entityTrader.activeQuests;
            var listIndex = 0;
            var currentDifficultyTier = 1;

            for (int i = 0; i < activeQuests.Count; i++)
            {
                // If Special quests as they are the Next Trader quests, QuestType will be empty for normal trader quests
                if (string.IsNullOrEmpty(activeQuests[i]?.QuestClass?.QuestType))
                {
                    var quest = activeQuests[i];
                    var name = quest.QuestClass.Name.ParseQuestName(quest.QuestClass.DifficultyTier);

                    // If weve gone up a Tier then reset the index
                    if(currentDifficultyTier != quest.QuestClass.DifficultyTier)
                    {
                        listIndex = 0;
                        currentDifficultyTier = quest.QuestClass.DifficultyTier;
                    }

                    Waypoint waypoint = new Waypoint();
                    waypoint.pos = new Vector3i(quest.Position);
                    waypoint.icon = WMMConstants.QuestWaypointIcon;
                    waypoint.entityId = listIndex; // Hijack entityId to store listIndex used to remove quests from the server!
                    waypoint.name = name;
                    xui.playerUI.entityPlayer.Waypoints.List.Add(waypoint);
                    waypoint.navObject = NavObjectManager.Instance.RegisterNavObject("waypoint", waypoint.pos.ToVector3(), waypoint.icon, true);
                    waypoint.navObject.IsActive = false;
                    waypoint.navObject.name = name;
                    waypoint.navObject.HiddenDisplayName = WMMConstants.HiddenDisplayName;
                    waypoint.navObject.usingLocalizationId = waypoint.bUsingLocalizationId;
                    waypoint.navObject.ExtraData = quest.QuestClass.DifficultyTier; // Hijack ExtraData to store Difficulty Tier
                    waypoint.navObject.hiddenOnCompass = false;
                    waypoint.navObject.UseOverrideColor = true;

                    listIndex++;

                    switch (quest.QuestClass.DifficultyTier)
                    {
                        //case 1:// #9C8867 = 156,136,103
                        //case 1:// # = 191, 172, 142 = new Color(0.749f, 0.674f, 0.556f);
                        case 1: // #ece3d4 = 236,227,212
                            waypoint.navObject.OverrideColor = new Color(0.925f, 0.890f, 0.831f);
                            break;
                        case 2: // #CF7F29 = 207,127,41
                            waypoint.navObject.OverrideColor = new Color(0.811f, 0.498f, 0.160f);
                            break;
                        case 3: // #A2A41B = 162,164,27
                            waypoint.navObject.OverrideColor = new Color(0.635f, 0.643f, 0.105f);
                            break;
                        case 4: // #42C234 = 66,194,52
                            waypoint.navObject.OverrideColor = new Color(0.258f, 0.760f, 0.203f);
                            break;
                        case 5: // #315DCE = 49,93,206
                            waypoint.navObject.OverrideColor = new Color(0.192f, 0.364f, 0.807f);
                            break;
                        case 6: // #A42ACC = 164,42,204
                            waypoint.navObject.OverrideColor = new Color(0.643f, 0.164f, 0.8f);
                            break;
                        case 7: // #52B9D1 = 82,185,209
                            waypoint.navObject.OverrideColor = new Color(0.321f, 0.725f, 0.819f);
                            break;
                        case 8: // #4E7841 = 78,120,65
                            waypoint.navObject.OverrideColor = new Color(0.305f, 0.470f, 0.254f);
                            break;
                        case 9: // #D366B2 = 211,102,178
                            waypoint.navObject.OverrideColor = new Color(0.827f, 0.4f, 0.698f);
                            break;
                        case 10: // #9F2620 = 159,38,32
                            waypoint.navObject.OverrideColor = new Color(0.623f, 0.149f, 0.090f);
                            break;

                    }
                }
            }

            // Set visibility of the buttons 
            var playerFactionTier = entityPlayer.QuestJournal.GetCurrentFactionTier(entityTrader.NPCInfo.QuestFaction, 0, false);

            tier1panel.ViewComponent.IsVisible = true;
            tier2panel.ViewComponent.IsVisible = playerFactionTier > 1;
            tier3panel.ViewComponent.IsVisible = playerFactionTier > 2;
            tier4panel.ViewComponent.IsVisible = playerFactionTier > 3;
            tier5panel.ViewComponent.IsVisible = playerFactionTier > 4;
            tier6panel.ViewComponent.IsVisible = playerFactionTier > 5;
            tier7panel.ViewComponent.IsVisible = playerFactionTier > 6;
            tier8panel.ViewComponent.IsVisible = playerFactionTier > 7;
            tier9panel.ViewComponent.IsVisible = playerFactionTier > 8;
            tier10panel.ViewComponent.IsVisible = playerFactionTier > 9;

            // Set the default toggle based on users previous toggling in this session
            SetDefaultToggle(btnTier1, 1);
            SetDefaultToggle(btnTier2, 2);
            SetDefaultToggle(btnTier3, 3);
            SetDefaultToggle(btnTier4, 4);
            SetDefaultToggle(btnTier5, 5);
            SetDefaultToggle(btnTier6, 6);
            SetDefaultToggle(btnTier7, 7);
            SetDefaultToggle(btnTier8, 8);
            SetDefaultToggle(btnTier9, 9);
            SetDefaultToggle(btnTier10, 10);
        }
    }

    private void SetDefaultToggle(XUiV_Button btn, int tier)
    {
        if (buttonToggles.ContainsKey(tier))
        {
            if (!buttonToggles[tier])
            {
                btn.Selected = false;
                ToggleNavObjectVisibility(tier);
                return;
            }
        }

        btn.Selected = true;
    }

    public override void OnClose()
    {
        base.OnClose();

        RemoveTraderQuestsFromMap();
    }

    private void RemoveTraderQuestsFromMap()
    {
        var entityPlayer = xui.playerUI.entityPlayer;
        var waypoints = entityPlayer.Waypoints.List;

        for (int i = waypoints.Count - 1; i >= 0; i--)
        {
            var waypoint = waypoints[i];

            if (waypoint.name != null
                && waypoint.navObject != null
                && waypoint.navObject.HiddenDisplayName == WMMConstants.HiddenDisplayName)
            {
                entityPlayer.Waypoints.List.RemoveAt(i);
                NavObjectManager.Instance.UnRegisterNavObject(waypoint.navObject);
            }
        }
    }

    private void SaveButtonToggle(int tier, bool value)
    {
        if(buttonToggles.ContainsKey(tier))
        {
            buttonToggles[tier] = value;
        }
        else
        {
            buttonToggles.Add(tier, value);
        }
    }
}