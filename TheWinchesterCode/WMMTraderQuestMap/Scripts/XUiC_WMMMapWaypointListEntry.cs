﻿using UnityEngine;

public class XUiC_WMMMapWaypointListEntry : XUiController
{
	public int Index;

	public XUiV_Sprite Background;

	public XUiV_Sprite Sprite;

	public XUiV_Label Name;

	public XUiV_Label Distance;

	public Waypoint Waypoint;

	private bool m_bSelected;

    private XUiC_WMMMapTraderQuests mapTraderQuestsWindow;

	private XUiC_WMMMapTraderQuestList questWaypointList;

	private XUiC_WMMMapArea mapArea;

	public override void Init()
	{
		base.Init();
		questWaypointList = (XUiC_WMMMapTraderQuestList)Parent.GetChildById("traderQuestList");
		Background = (XUiV_Sprite)GetChildById("Background").ViewComponent;
		Sprite = (XUiV_Sprite)GetChildById("Icon").ViewComponent;
		Name = (XUiV_Label)GetChildById("Name").ViewComponent;
		Distance = (XUiV_Label)GetChildById("Distance").ViewComponent;
        Background.Controller.OnPress += Controller_OnPress;
		Background.Controller.OnHover += Controller_OnHover;
		mapTraderQuestsWindow = (XUiC_WMMMapTraderQuests)xui.GetWindow("mapTraderQuests").Controller;
		mapArea = (XUiC_WMMMapArea)xui.GetWindow("mapAreaTrader").Controller;
	}

    private void Controller_OnPress(XUiController _sender, int _mouseButton)
    {
		if (Waypoint != null)
		{
			mapArea.PositionMapAt(Waypoint.pos.ToVector3());

			// Move to waypoint on the map
			var quest = WMMTraderQuestMapCore.GetQuestFromPosition(Waypoint.pos);
			if (quest != null)
			{
				Selected = true;
				mapTraderQuestsWindow.SelectQuestWaypoint(quest, Waypoint.entityId);
				questWaypointList.UpdateWaypointsList(false, Waypoint);
			}
		}
	}

	private void Controller_OnHover(XUiController _sender, bool _isOver)
	{
		updateSelected(Waypoint != null && _isOver);
	}

	public new bool Selected
	{
		get
		{
			return m_bSelected;
		}
		set
		{
			m_bSelected = value;
			updateSelected(false);
		}
	}

	private void updateSelected(bool _bHover)
	{
		XUiV_Sprite background = Background;
		if (background != null)
		{
			if (m_bSelected)
			{
				background.Color = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
				background.SpriteName = "ui_game_select_row";
			}
			else if (_bHover)
			{
				background.Color = new Color32(96, 96, 96, byte.MaxValue);
				background.SpriteName = "menu_empty";
			}
			else
			{
				background.Color = new Color32(64, 64, 64, byte.MaxValue);
				background.SpriteName = "menu_empty";
			}
		}
	}
}