﻿public class XUiC_WMMMapTraderQuests : XUiController
{
    public XUiV_Label QuestName;

    public XUiV_Label QuestDifficulty;

    public XUiV_Label DescriptionText;

    private XUiController btnAccept;

    private XUiV_Button btnAccept_Background;

    private Quest SelectedQuest;

    private int ListIndex;

    public override void Init()
    {
        base.Init();

        QuestName = (XUiV_Label)GetChildById("questname").ViewComponent;
        QuestDifficulty = (XUiV_Label)GetChildById("questdifficulty").ViewComponent;
        DescriptionText = (XUiV_Label)GetChildById("descriptionText").ViewComponent;

        btnAccept = GetChildById("btnAccept");
        btnAccept_Background = (XUiV_Button)btnAccept.GetChildById("clickable").ViewComponent;
        btnAccept_Background.Controller.OnPress += btnAccept_OnPress;
    }

    public void SelectQuestWaypoint(Quest quest, int listIndex)
    {
        SelectedQuest = quest;
        QuestName.Text = quest.QuestClass.Name.ParseQuestName(quest.QuestClass.DifficultyTier);
        QuestDifficulty.Text = quest.QuestClass.Difficulty;
        DescriptionText.Text = quest.GetParsedText(quest.QuestClass.Offer);
        ListIndex = listIndex;
    }

    private void btnAccept_OnPress(XUiController _sender, int _mouseButton)
    {
        if (SelectedQuest == null)
        {
            // Log.Out("XUiC_WMMMapWaypointListEntry : Cannot find Quest at position x:" + Waypoint.pos.x + " z:" + Waypoint.pos.z);

            return;
        }

        if (SelectedQuest.QuestGiverID != -1)
        {
            xui.Dialog.Respondent.PlayVoiceSetEntry("questaccepted", true, true);
        }

        AcceptQuest(SelectedQuest);
        xui.playerUI.windowManager.Close(WindowGroup.ID);
    }

    private void AcceptQuest(Quest quest)
    {
        if (quest.QuestGiverID != -1)
        {
            xui.Dialog.Respondent.PlayVoiceSetEntry("questaccepted", true, true);
        }

        (xui.Dialog.Respondent as EntityTrader).activeQuests.Remove(quest);
        Log.Out("ListIndex ListIndex : " + ListIndex);

        if (!SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer)
        {
            SingletonMonoBehaviour<ConnectionManager>.Instance.SendToServer(NetPackageManager.GetPackage<NetPackageNPCQuestList>().Setup(quest.QuestGiverID, xui.playerUI.entityPlayer.entityId, quest.QuestClass.DifficultyTier, (byte)ListIndex), false);
        }

        xui.playerUI.entityPlayer.QuestJournal.AddQuest(quest);
    }
}