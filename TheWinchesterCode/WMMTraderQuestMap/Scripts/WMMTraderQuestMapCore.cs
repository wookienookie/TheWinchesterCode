﻿using System.Linq;

public static class WMMTraderQuestMapCore
{
	public static Quest GetQuestFromPosition(Vector3i pos)
	{
		LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(GameManager.Instance.World.GetPrimaryPlayer());
		EntityTrader entityTrader = uiforPlayer.xui.Dialog.Respondent as EntityTrader;

		if (entityTrader != null)
		{
			var activeQuests = entityTrader.activeQuests;

			for (int i = 0; i < activeQuests.Count; i++)
			{
				if (activeQuests[i].Position.x == pos.x
					&& activeQuests[i].Position.z == pos.z)
				{
					return activeQuests[i];
				}
			}
		}

		return null;
	}

	public static string ParseQuestName(this string name, byte difficultyTier)
	{
		if (name.StartsWith("Tier "))
		{
			var split = name.Split(' ');

			if (split.Length > 2)
			{
				var data = split.ToList();
				data.RemoveRange(0, 2);
				return string.Format("T{0} {1}", difficultyTier, string.Join(" ", data));
			}
		}

		return name;
	}
}