﻿using System;
using HarmonyLib;
using System.Reflection;
using UnityEngine;

public class WMMTraderQuestMap_Init : IModApi
{
    public void InitMod(Mod _modInstance)
    {
        Log.Out(" Loading Patch: " + this.GetType().ToString());

        // Reduce extra logging stuff
        Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
        Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

        var harmony = new HarmonyLib.Harmony(GetType().ToString());
        harmony.PatchAll(Assembly.GetExecutingAssembly());
    }
}

[HarmonyPatch(typeof(XUiC_DialogResponseList))]
[HarmonyPatch("OnPressResponse")]
[HarmonyPatch(new Type[] { typeof(XUiController), typeof(int) })]
public class PatchXUiC_DialogResponseListOnPressResponse
{
    static bool Prefix(XUiC_DialogResponseList __instance, XUiController _sender)
    {
        var entry = (XUiC_DialogResponseEntry)_sender;
        var currentResponse = AccessTools.Field(typeof(XUiC_DialogResponseEntry), "currentResponse").GetValue(entry) as DialogResponse;

        var windowGroup = AccessTools.Field(typeof(XUiController), "windowGroup").GetValue(__instance) as XUiWindowGroup;
        var mapArea = windowGroup.Controller.GetChildByType<XUiC_WMMMapArea>();
        var traderQuests = windowGroup.Controller.GetChildByType<XUiC_WMMMapTraderQuests>();

        // Show/hide the new WMMMapArea for the Trader Quest Dialog only
        var showMap = currentResponse.ID.StartsWith("jobshave")
            || currentResponse.ID.StartsWith("jobsprev")
            || currentResponse.ID.StartsWith("jobsnext");
        mapArea.ViewComponent.IsVisible = showMap;
        traderQuests.ViewComponent.IsVisible = showMap;

        return true;
    }
}

[HarmonyPatch(typeof(GUIWindowManager))]
[HarmonyPatch("OpenIfNotOpen")]
[HarmonyPatch(new Type[] { typeof(string), typeof(bool), typeof(bool), typeof(bool) })]
public class PatchGUIWindowManagerOpenIfNotOpen
{
    static bool Prefix(GUIWindowManager __instance, string _windowName, bool _bModal, bool _bIsNotEscClosable, bool _bCloseAllOpenWindows)
    {
        if (GameManager.Instance.World != null)
        {
            var uiforPlayer = LocalPlayerUI.GetUIForPlayer(GameManager.Instance.World.GetPrimaryPlayer());

            // Hack for an error....
            if (_windowName == "windowpaging" && uiforPlayer.xui.Dialog.Respondent != null)
            {
                return false;
            }
        }

        return true;
    }
}

[HarmonyPatch(typeof(WaypointCollection))]
[HarmonyPatch("GetWaypointForNavObject")]
[HarmonyPatch(new Type[] { typeof(NavObject)})]
public class Patch_WaypointCollection_GetWaypointForNavObject
{
    static void Postfix(WaypointCollection __instance, ref Waypoint __result, ref NavObject nav)
    {
        // Hijack the on click on waypoints on the map!
        if (nav.HiddenDisplayName == WMMConstants.HiddenDisplayName
            && __result != null)
        {
            var quest = WMMTraderQuestMapCore.GetQuestFromPosition(__result.pos);
            if (quest != null)
            {
                var uiforPlayer = LocalPlayerUI.GetUIForPlayer(GameManager.Instance.World.GetPrimaryPlayer());
                var mapTraderQuestsWindow = (XUiC_WMMMapTraderQuests)uiforPlayer.xui.GetWindow("mapTraderQuests").Controller;

                mapTraderQuestsWindow.SelectQuestWaypoint(quest, __result.entityId);

                var mapTraderQuestList = mapTraderQuestsWindow.GetChildByType<XUiC_WMMMapTraderQuestList>();

                if(mapTraderQuestList != null)
                {
                    mapTraderQuestList.currentPage = quest.QuestClass.DifficultyTier - 1;
                    mapTraderQuestList.UpdateWaypointsList(false, __result);
                }
            }
        }
    }
}

[HarmonyPatch(typeof(NavObject), "HasRequirements", MethodType.Getter)]
public class Patch_NavObject_HasRequirements
{
    static bool Prefix(NavObject __instance, bool __result)
    {
        // See XUiC_WMMMapArea.ToggleNavObjectVisibility() how this horrible hack works!
        if (__instance.HiddenDisplayName == WMMConstants.HiddenDisplayName
            && !__instance.HasOnScreen)
        {
            __result = false;
            return false;
        }

        return true;
    }
}
