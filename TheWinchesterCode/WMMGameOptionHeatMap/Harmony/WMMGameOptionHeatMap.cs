﻿//using System;
//using HarmonyLib;
//using UnityEngine;
//using System.Reflection;
//using System.Collections.Generic;

//public class WMMGameOptionHeatMap
//{
//    public class WMMGameOptionHeatMap_Init : IModApi
//    {
//        public void InitMod(Mod _modInstance)
//        {
//            Log.Out(" Loading Patch: " + this.GetType().ToString());

//            // Reduce extra logging stuff
//            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
//            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

//            var harmony = new HarmonyLib.Harmony(GetType().ToString());
//            harmony.PatchAll(Assembly.GetExecutingAssembly());
//        }
//    }

//    [HarmonyPatch(typeof(AIDirectorChunkEventComponent))]
//    [HarmonyPatch("checkHordeLevel")]
//    [HarmonyPatch(new Type[] { typeof(AIDirectorChunkData) })]
//    public class PatchAIDirectorChunkEventComponentcheckHordeLevel
//    {
//        static bool Prefix(AIDirectorChunkEventComponent __instance, AIDirectorChunkData _chunkData)
//        {
//            if (GameStats.GetBool(EnumGameStats.ZombieHordeMeter) && GameStats.GetBool(EnumGameStats.IsSpawnEnemies))
//            {
//                AIDirector director = __instance.Director;
//                _chunkData.ExpireEvents(director.World.worldTime);
//                if (_chunkData.ActivityLevel >= WMMCustomGameOptions.GetInt("HeatMap"))
//                {
//                    AIDirectorChunkEvent mostInterestingEvent = _chunkData.GetMostInterestingEvent();
//                    _chunkData.ClearEvents();
//                    if (director.random.RandomFloat < 0.6f && !GameUtils.IsPlaytesting())
//                    {
//                        if (mostInterestingEvent != null)
//                        {
//                            __instance.SpawnScouts(new Vector3((float)mostInterestingEvent.Position.x, (float)mostInterestingEvent.Position.y, (float)mostInterestingEvent.Position.z));
//                            return false;
//                        }
//                        Log.Out("AIDirector: Error chunk heat map had no event of interest!");
//                    }
//                }
//            }

//            return false;
//        }
//    }
//}
