﻿using System;
using HarmonyLib;
using System.Reflection;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Linq;
using UnityEngine;
using System.Globalization;

public class WMMQualityRework
{
    private static bool QualityInfoLoaded = false;
    public static GameRandom Random;

    public class WMMQualityRework_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());

            Random = GameRandomManager.Instance.CreateGameRandom();
        }
    }

    [HarmonyPatch(typeof(ItemValue), MethodType.Constructor)]
    [HarmonyPatch("ItemValue")]
    [HarmonyPatch(new Type[] { typeof(int), typeof(bool) })]
    class PatchItemValueItemValue
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            var codes = new List<CodeInstruction>(instructions);

            for (int i = 0; i < codes.Count; i++)
            {
                if (codes[i].opcode == OpCodes.Ldc_I4_6)
                {
                    codes[i].opcode = OpCodes.Ldc_I4;
                    codes[i].operand = 1000;
                    break;
                }
            }
            return codes.AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(QualityInfo))]
    [HarmonyPatch("GetTierColor")]
    [HarmonyPatch(new Type[] { typeof(int) })]
    class PatchQualityInfoGetTierColor
    {
        static bool Prefix(QualityInfo __instance, ref int _tier)
        {
            if (_tier > 0)
            {
                _tier = (int)Math.Floor(_tier / 100 + 1m);
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(QualityInfo))]
    [HarmonyPatch("GetQualityColorHex")]
    [HarmonyPatch(new Type[] { typeof(int) })]
    class PatchQualityInfoGetQualityColorHex
    {
        static bool Prefix(QualityInfo __instance, ref int _quality)
        {
            if (_quality > 0)
            {
                _quality = (int)Math.Floor(_quality / 100 + 1m);
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(XUiC_RecipeStack))]
    [HarmonyPatch("SetRecipe")]
    [HarmonyPatch(new Type[] { typeof(Recipe), typeof(int), typeof(float), typeof(bool), typeof(int), typeof(int), typeof(float) })]
    class PatchXUiC_RecipeStackSetRecipe
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            var codes = new List<CodeInstruction>(instructions);

            for (int i = 0; i < codes.Count; i++)
            {
                if (codes[i].opcode == OpCodes.Ldc_R4 && (float)codes[i].operand == 6f)
                {
                    codes[i].operand = 1000f;
                    break;
                }
            }
            return codes.AsEnumerable();
        }
    }

    // This change covers 2 of the original changes in this class TraderInfo
    [HarmonyPatch(typeof(TraderInfo))]
    [HarmonyPatch("applyQuality")]
    [HarmonyPatch(new Type[] { typeof(ItemValue), typeof(int), typeof(int) }, new ArgumentType[] { ArgumentType.Ref, ArgumentType.Normal, ArgumentType.Normal })]
    class PatchTraderInfoapplyQuality
    {
        static bool Prefix(TraderInfo __instance, ref ItemValue _itemValue, int minQuality, ref int maxQuality)
        {
            if (maxQuality == 6)
                maxQuality = 1000;

            return true;
        }
    }

    [HarmonyPatch(typeof(XUiM_Trader))]
    [HarmonyPatch("GetBuyPrice")]
    [HarmonyPatch(new Type[] { typeof(XUi), typeof(ItemValue), typeof(int), typeof(ItemClass), typeof(int) })]
    class PatchXUiM_TraderGetBuyPrice
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            var codes = new List<CodeInstruction>(instructions);

            for (int i = 0; i < codes.Count; i++)
            {
                if (codes[i].opcode == OpCodes.Ldc_R4 && (float)codes[i].operand == 5f)
                {
                    codes[i].operand = 1000f;
                    break;
                }
            }
            return codes.AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(XUiM_Trader))]
    [HarmonyPatch("GetSellPrice")]
    [HarmonyPatch(new Type[] { typeof(XUi), typeof(ItemValue), typeof(int), typeof(ItemClass) })]
    class PatchXUiM_TraderGetSellPrice
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            var codes = new List<CodeInstruction>(instructions);

            for (int i = 0; i < codes.Count; i++)
            {
                if (codes[i].opcode == OpCodes.Ldc_R4 && (float)codes[i].operand == 5f)
                {
                    codes[i].operand = 1000f;
                    break;
                }
            }
            return codes.AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(ItemValue), MethodType.Constructor)]
    [HarmonyPatch("ItemValue")]
    [HarmonyPatch(new Type[] { typeof(int), typeof(int), typeof(int), typeof(bool), typeof(string[]), typeof(float) })]
    class PatchItemValueItemValue2
    {
        static void Postfix(ItemValue __instance)
        {
            if (__instance.HasQuality)
            {
                if (__instance.Quality >= 800)
                {
                    __instance.Modifications = new ItemValue[4];
                }
                else if (__instance.Quality >= 600)
                {
                    __instance.Modifications = new ItemValue[3];
                }
                else if (__instance.Quality >= 400)
                {
                    __instance.Modifications = new ItemValue[2];
                }
                else if (__instance.Quality >= 200)
                {
                    __instance.Modifications = new ItemValue[1];
                }
                else
                    __instance.Modifications = new ItemValue[0];
            }
        }
    }

    [HarmonyPatch(typeof(QualityInfo))]
    [HarmonyPatch("Add")]
    [HarmonyPatch(new Type[] { typeof(int), typeof(string) })]
    class PatchQualityInfoAdd
    {
        static bool Prefix(QualityInfo __instance, ref int _key, ref string _hexColor)
        {
            if (!QualityInfoLoaded)
            {
                QualityInfo.Cleanup();
                QualityInfoLoaded = true;
                Log.Out("WMM QualityInfo loaded");
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(QualityInfo))]
    [HarmonyPatch("Cleanup")]
    [HarmonyPatch(new Type[] { })]
    class PatchQualityInfoCleanup
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            var codes = new List<CodeInstruction>(instructions);

            for (int i = 0; i < codes.Count; i++)
            {
                if (codes[i].opcode == OpCodes.Ldc_I4_7)
                {
                    codes[i].opcode = OpCodes.Ldc_I4;
                    codes[i].operand = 11;
                }
            }
            return codes.AsEnumerable();
        }
    }

    #region This is for the Quality greater than 255 bug in workstations.
    [HarmonyPatch(typeof(XUiC_WorkstationWindowGroup))]
    [HarmonyPatch("syncTEfromUI")]
    [HarmonyPatch()]
    class PatchXUiC_WorkstationWindowGroupsyncTEfromUI
    {
        static bool Prefix(XUiC_WorkstationWindowGroup __instance, XUiC_WorkstationToolGrid ___toolWindow, XUiC_WorkstationInputGrid ___inputWindow, XUiC_WorkstationOutputGrid ___outputWindow, XUiC_WorkstationFuelGrid ___fuelWindow, XUiC_CraftingQueue ___craftingQueue)
        {
            if (___toolWindow != null)
            {
                __instance.WorkstationData.SetToolStacks(___toolWindow.GetSlots());
            }

            if (___inputWindow != null)
            {
                __instance.WorkstationData.SetInputStacks(___inputWindow.GetSlots());
            }

            if (___outputWindow != null)
            {
                __instance.WorkstationData.SetOutputStacks(___outputWindow.GetSlots());
            }

            if (___fuelWindow != null)
            {
                __instance.WorkstationData.SetFuelStacks(___fuelWindow.GetSlots());
            }

            if (___craftingQueue != null)
            {
                XUiC_RecipeStack[] recipesToCraft = ___craftingQueue.GetRecipesToCraft();
                RecipeQueueItem[] array = new RecipeQueueItem[recipesToCraft.Length];
                for (int i = 0; i < recipesToCraft.Length; i++)
                {
                    var recipeQueueItem = new RecipeQueueItem
                    {
                        Recipe = recipesToCraft[i].GetRecipe(),
                        Multiplier = (short)recipesToCraft[i].GetRecipeCount(),
                        CraftingTimeLeft = recipesToCraft[i].GetRecipeCraftingTimeLeft(),
                        IsCrafting = recipesToCraft[i].IsCrafting,
                        Quality = (byte)recipesToCraft[i].OutputQuality,
                        StartingEntityId = recipesToCraft[i].StartingEntityId,
                        OneItemCraftTime = recipesToCraft[i].GetOneItemCraftTime()
                    };

                    if (recipeQueueItem.RepairItem == null || recipeQueueItem.RepairItem.type == 0)
                        recipeQueueItem.StartingEntityId = 2000000000 + recipesToCraft[i].OutputQuality;

                    array[i] = recipeQueueItem;
                }
                __instance.WorkstationData.SetRecipeQueueItems(array);
            }
            __instance.WorkstationData.TileEntity.ResetTickTime();

            return false;
        }
    }

    [HarmonyPatch(typeof(TileEntityWorkstation))]
    [HarmonyPatch("HandleRecipeQueue")]
    [HarmonyPatch(new Type[] { typeof(float) })]
    class PatchTileEntityWorkstationHandleRecipeQueue
    {
        static bool Prefix(TileEntityWorkstation __instance, float _timePassed, bool ___bUserAccessing, RecipeQueueItem[] ___queue, bool ___isBurning, bool[] ___isModuleUsed, ItemStack[] ___output)
        {
            if (___bUserAccessing)
            {
                return false;
            }

            if (___queue.Length == 0)
            {
                return false;
            }

            if (___isModuleUsed[3] && !___isBurning)
            {
                return false;
            }

            RecipeQueueItem recipeQueueItem = ___queue[___queue.Length - 1];
            if (recipeQueueItem == null)
            {
                return false;
            }
            if (recipeQueueItem.CraftingTimeLeft >= 0f)
            {
                recipeQueueItem.CraftingTimeLeft -= _timePassed;
            }

            Traverse hasRecipeInQueueMethod = Traverse.Create(__instance).Method("hasRecipeInQueue", Array.Empty<object>());
            while (recipeQueueItem.CraftingTimeLeft < 0f && hasRecipeInQueueMethod.GetValue<bool>())
            {
                if (recipeQueueItem.Multiplier > 0)
                {
                    ItemValue itemValue = new ItemValue(recipeQueueItem.Recipe.itemValueType, false);
                    if (ItemClass.list[recipeQueueItem.Recipe.itemValueType] != null && ItemClass.list[recipeQueueItem.Recipe.itemValueType].HasQuality)
                    {
                        var quality = (int)recipeQueueItem.Quality;
                        if (recipeQueueItem.StartingEntityId > 2000000000)
                            quality = recipeQueueItem.StartingEntityId - 2000000000;

                        itemValue = new ItemValue(recipeQueueItem.Recipe.itemValueType, quality, quality, false, null, 1f);
                    }

                    if (ItemStack.AddToItemStackArray(___output, new ItemStack(itemValue, recipeQueueItem.Recipe.count), -1) == -1)
                    {
                        return false;
                    }
                    GameSparksCollector.IncrementCounter(GameSparksCollector.GSDataKey.CraftedItems, itemValue.ItemClass.Name, 1, true, GameSparksCollector.GSDataCollection.SessionUpdates);
                    RecipeQueueItem recipeQueueItem2 = recipeQueueItem;
                    recipeQueueItem2.Multiplier -= 1;
                    recipeQueueItem.CraftingTimeLeft += recipeQueueItem.OneItemCraftTime;
                }
                if (recipeQueueItem.Multiplier <= 0)
                {
                    float craftingTimeLeft = recipeQueueItem.CraftingTimeLeft;
                    Traverse cycleRecipeQueueMethod = Traverse.Create(__instance).Method("cycleRecipeQueue", Array.Empty<object>());
                    cycleRecipeQueueMethod.GetValue();
                    recipeQueueItem = ___queue[___queue.Length - 1];
                    recipeQueueItem.CraftingTimeLeft += ((craftingTimeLeft < 0f) ? craftingTimeLeft : 0f);
                }
            }

            return false;
        }
    }

    [HarmonyPatch(typeof(XUiC_CraftingQueue))]
    [HarmonyPatch("AddRecipeToCraftAtIndex")]
    [HarmonyPatch(new Type[] { typeof(int), typeof(Recipe), typeof(int), typeof(float), typeof(bool), typeof(bool), typeof(int), typeof(int), typeof(float) })]
    class PatchXUiC_CraftingQueueAddRecipeToCraftAtIndex
    {
        static bool Prefix(XUiC_CraftingQueue __instance, ref bool __result, int _index, Recipe _recipe, XUiController[] ___queueItems, int _count = 1, float craftTime = -1f, bool isCrafting = true, bool recipeModification = false, int lastQuality = -1, int startingEntityId = -1, float _oneItemCraftingTime = -1f)
        {
            XUiC_RecipeStack xuiC_RecipeStack = (XUiC_RecipeStack)___queueItems[_index];
            if (xuiC_RecipeStack.SetRecipe(_recipe, _count, craftTime, recipeModification, -1, -1, _oneItemCraftingTime))
            {
                // startingEntityId is the EntityId of the Player that added the item to the queue, this is so they get XP. We dont care about this so going to hijack this variable BUT the value could be anything from zero up.  Im going to hijack any values above 2 billion and use that as a base for the quality.
                if (startingEntityId > 2000000000)
                    lastQuality = startingEntityId - 2000000000;

                xuiC_RecipeStack.IsCrafting = (_index == ___queueItems.Length - 1 && isCrafting);
                if (lastQuality != -1)
                {
                    xuiC_RecipeStack.OutputQuality = lastQuality;
                }
                if (startingEntityId != -1)
                {
                    xuiC_RecipeStack.StartingEntityId = startingEntityId;
                }

                xuiC_RecipeStack.IsDirty = true;
                __result = true;
                return false;
            }
            __result = false;

            return false;
        }
    }

    [HarmonyPatch(typeof(XUi))]
    [HarmonyPatch("GetCraftingData")]
    [HarmonyPatch(new Type[] { })]
    class PatchXUiGetCraftingData
    {
        static bool Prefix(XUi __instance, ref CraftingData __result)
        {
            CraftingData craftingData = new CraftingData();
            XUiController xuiController = __instance.FindWindowGroupByName("crafting");
            if (xuiController == null)
            {
                __result = craftingData;
                return false;
            }
            XUiC_CraftingQueue childByType = xuiController.GetChildByType<XUiC_CraftingQueue>();
            if (childByType == null)
            {
                __result = craftingData;
                return false;
            }
            XUiC_RecipeStack[] recipesToCraft = childByType.GetRecipesToCraft();
            if (recipesToCraft == null)
            {
                __result = craftingData;
                return false;
            }
            craftingData.RecipeQueueItems = new RecipeQueueItem[recipesToCraft.Length];
            for (int i = 0; i < recipesToCraft.Length; i++)
            {
                RecipeQueueItem recipeQueueItem = new RecipeQueueItem();
                recipeQueueItem.Recipe = recipesToCraft[i].GetRecipe();
                recipeQueueItem.Multiplier = (short)recipesToCraft[i].GetRecipeCount();
                recipeQueueItem.CraftingTimeLeft = recipesToCraft[i].GetRecipeCraftingTimeLeft();
                recipeQueueItem.IsCrafting = recipesToCraft[i].IsCrafting;
                recipeQueueItem.Quality = (byte)recipesToCraft[i].OutputQuality;
                recipeQueueItem.StartingEntityId = recipesToCraft[i].StartingEntityId;
                if (recipeQueueItem.RepairItem == null || recipeQueueItem.RepairItem.type == 0)
                {
                    recipeQueueItem.StartingEntityId = 2000000000 + recipesToCraft[i].OutputQuality;
                }
                recipeQueueItem.RepairItem = recipesToCraft[i].OriginalItem;
                recipeQueueItem.AmountToRepair = (ushort)recipesToCraft[i].AmountToRepair;
                recipeQueueItem.OneItemCraftTime = recipesToCraft[i].GetOneItemCraftTime();
                craftingData.RecipeQueueItems[i] = recipeQueueItem;
            }

            __result = craftingData;
            return false;
        }
    }


    #endregion

    // This patch used to be in a separate folder/patch : RH_FixMaximumItemQuality , not sure why?!
    [HarmonyPatch(typeof(EffectManager))]
    [HarmonyPatch("GetValue")]
    [HarmonyPatch(new Type[] { typeof(PassiveEffects), typeof(ItemValue), typeof(float), typeof(EntityAlive), typeof(Recipe), typeof(FastTags), typeof(bool), typeof(bool), typeof(bool), typeof(bool), typeof(int), typeof(bool), typeof(bool) })]
    public class PatchEffectManagerGetValue
    {
        static void Postfix(ref float __result, ref PassiveEffects _passiveEffect, ref ItemValue _originalItemValue, ref float _originalValue, ref EntityAlive _entity, ref Recipe _recipe, ref FastTags tags, ref bool calcEquipment, ref bool calcHoldingItem, ref bool calcProgression, ref bool calcBuffs, ref int craftingTier, ref bool useMods)
        {
            if (_passiveEffect == PassiveEffects.CraftingTier)
            {
                __result = Math.Min(__result, 1000f);
            }
        }
    }

    // Patch to fix quality in Bundles.
    [HarmonyPatch(typeof(ItemActionOpenBundle))]
    [HarmonyPatch("ExecuteInstantAction")]
    [HarmonyPatch(new Type[] { typeof(EntityAlive), typeof(ItemStack), typeof(bool), typeof(XUiC_ItemStack) })]
    class PatchItemActionOpenBundleExecuteInstantAction
    {
        static bool Prefix(ItemActionOpenBundle __instance, ref bool __result, ref EntityAlive ent, ref ItemStack stack, ref bool isHeldItem, ref XUiC_ItemStack stackController, ref string ___soundStart)
        {
            ent.MinEventContext.ItemValue = stack.itemValue;
            ent.MinEventContext.ItemValue.FireEvent(MinEventTypes.onSelfPrimaryActionStart, ent.MinEventContext);
            ent.FireEvent(MinEventTypes.onSelfPrimaryActionStart, false);
            if (___soundStart != null)
            {
                ent.PlayOneShot(___soundStart, false);
            }
            if (__instance.Consume)
            {
                if (stack.itemValue.MaxUseTimes > 0 && stack.itemValue.UseTimes + 1f < (float)stack.itemValue.MaxUseTimes)
                {
                    stack.itemValue.UseTimes += EffectManager.GetValue(PassiveEffects.DegradationPerUse, stack.itemValue, 1f, ent, null, stack.itemValue.ItemClass.ItemTags, true, true, true, true, 1, true);
                    __result = true;
                    return false;
                }
                if (isHeldItem)
                {
                    ent.inventory.DecHoldingItem(1);
                }
                else
                {
                    stack.count--;
                }
            }
            ent.MinEventContext.ItemValue = stack.itemValue;
            ent.MinEventContext.ItemValue.FireEvent(MinEventTypes.onSelfPrimaryActionEnd, ent.MinEventContext);
            ent.FireEvent(MinEventTypes.onSelfPrimaryActionEnd, false);
            if (__instance.CreateItem != null && __instance.CreateItemCount != null)
            {
                for (int i = 0; i < __instance.CreateItem.Length; i++)
                {
                    string text = (__instance.CreateItemCount != null && __instance.CreateItemCount.Length > i) ? __instance.CreateItemCount[i] : "1";
                    ItemClass itemClass = ItemClass.GetItemClass(__instance.CreateItem[i], false);
                    int num;
                    if (text.Contains("-"))
                    {
                        string[] array = text.Split(new char[]
                        {
                        '-'
                        });
                        int min = StringParsers.ParseSInt32(array[0], 0, -1, NumberStyles.Integer);
                        int maxExclusive = StringParsers.ParseSInt32(array[1], 0, -1, NumberStyles.Integer) + 1;
                        num = ent.rand.RandomRange(min, maxExclusive);
                    }
                    else
                    {
                        num = StringParsers.ParseSInt32(text, 0, -1, NumberStyles.Integer);
                    }
                    ItemValue itemValue;
                    if (itemClass.HasQuality)
                    {
                        num = GetUpdatedQuality(num);
                        itemValue = new ItemValue(itemClass.Id, num, num, false, null, 1f);
                        num = 1;
                    }
                    else
                    {
                        itemValue = new ItemValue(itemClass.Id, false);
                    }
                    ItemStack itemStack = new ItemStack(itemValue, num);
                    if (!LocalPlayerUI.GetUIForPlayer(ent as EntityPlayerLocal).xui.PlayerInventory.AddItem(itemStack))
                    {
                        ent.world.gameManager.ItemDropServer(itemStack, ent.GetPosition(), Vector3.zero, -1, 60f, false);
                    }
                }
            }
            if (__instance.RandomItem != null && __instance.RandomItemCount != null)
            {
                List<int> list = null;
                if (__instance.UniqueRandomOnly)
                {
                    list = new List<int>();
                    for (int j = 0; j < __instance.RandomItem.Length; j++)
                    {
                        list.Add(j);
                    }
                    for (int k = 0; k < list.Count * 3; k++)
                    {
                        int num2 = ent.rand.RandomRange(0, list.Count);
                        int num3 = ent.rand.RandomRange(0, list.Count);
                        if (num2 != num3)
                        {
                            int value = list[num2];
                            list[num2] = list[num3];
                            list[num3] = value;
                        }
                    }
                }
                int num4 = -1;
                for (int l = 0; l < __instance.RandomCount; l++)
                {
                    int num5;
                    if (__instance.UniqueRandomOnly)
                    {
                        num4++;
                        if (num4 >= __instance.RandomItem.Length)
                        {
                            num4 = 0;
                        }
                        num5 = list[num4];
                    }
                    else
                    {
                        num5 = ent.rand.RandomRange(0, __instance.RandomItem.Length);
                    }
                    string text2 = (__instance.RandomItemCount != null && __instance.RandomItemCount.Length > l) ? __instance.RandomItemCount[l] : "1";
                    ItemClass itemClass2 = ItemClass.GetItemClass(__instance.RandomItem[num5], false);
                    int num6;
                    if (text2.Contains("-"))
                    {
                        string[] array2 = text2.Split(new char[]
                        {
                        '-'
                        });
                        int min2 = StringParsers.ParseSInt32(array2[0], 0, -1, NumberStyles.Integer);
                        int maxExclusive2 = StringParsers.ParseSInt32(array2[1], 0, -1, NumberStyles.Integer) + 1;
                        num6 = ent.rand.RandomRange(min2, maxExclusive2);
                    }
                    else
                    {
                        num6 = StringParsers.ParseSInt32(text2, 0, -1, NumberStyles.Integer);
                    }
                    ItemValue itemValue2;
                    if (itemClass2.HasQuality)
                    {
                        num6 = GetUpdatedQuality(num6);
                        itemValue2 = new ItemValue(itemClass2.Id, num6, num6, false, null, 1f);
                        num6 = 1;
                    }
                    else
                    {
                        itemValue2 = new ItemValue(itemClass2.Id, false);
                    }
                    ItemStack itemStack2 = new ItemStack(itemValue2, num6);
                    if (!LocalPlayerUI.GetUIForPlayer(ent as EntityPlayerLocal).xui.PlayerInventory.AddItem(itemStack2))
                    {
                        ent.world.gameManager.ItemDropServer(itemStack2, ent.GetPosition(), Vector3.zero, -1, 60f, false);
                    }
                }
            }

            __result = true;
            return false;
        }

        private static int GetUpdatedQuality(int originalQuality)
        {
            switch (originalQuality)
            {
                case 1:
                    return Random.RandomRange(50,100);
                case 2:
                    return Random.RandomRange(100, 200);
                case 3:
                    return Random.RandomRange(200, 400);
                case 4:
                    return Random.RandomRange(400, 600);
                case 5:
                    return Random.RandomRange(600, 800);
                case 6:
                    return Random.RandomRange(800, 1000);
            }

            return 1;
        }
    }
}
