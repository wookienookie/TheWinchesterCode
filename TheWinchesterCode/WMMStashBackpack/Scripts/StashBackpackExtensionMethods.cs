﻿using System.Collections.Generic;

public static class StashBackpackExtensionMethods
{
    public static bool GetBool(this Dictionary<string, string> dictionary, string key)
    {
        if (dictionary.ContainsKey(key))
        {
            var output = false;
            bool.TryParse(dictionary[key], out output);
            return output;
        }

        return false;
    }

    public static void SetBool(this Dictionary<string, string> dictionary, string key, bool value)
    {
        if (dictionary.ContainsKey(key))
            dictionary[key] = value.ToString();
        else
            dictionary.Add(key, value.ToString());
    }
}