﻿using System;
using System.Collections.Generic;
using System.IO;

public static class StashBackpackCore
{
    private static string _fullPath = "";
    private static string _filename = "StashBackpack.txt";
    public static ulong StashBackpackXMarkedTime = 0;
    public static int BagSize = 0;

    public static void SaveStashBackpack(List<bool> bagLockedSlots)
    {
        if (bagLockedSlots.Count == 0)
        {
            for (int i = 0; i < BagSize; i++)
            {
                bagLockedSlots.Add(false);
            }
        }

        Directory.CreateDirectory(FullPath);

        using (StreamWriter sw = File.CreateText(FullPathAndFilename))
        {
            sw.WriteLine(SimpleJson2.SimpleJson2.SerializeObject(bagLockedSlots));
        }
    }

    public static List<bool> LoadStashBackpack()
    {
        var bagSlots = new List<bool>();
        if (File.Exists(FullPathAndFilename))
        {
            try
            {
                var text = File.ReadAllText(FullPathAndFilename);
                bagSlots = SimpleJson2.SimpleJson2.DeserializeObject<List<bool>>(text);
            }
            catch (Exception exc)
            {
                Log.Out("Error reading LoadStashBackpack : " + exc.ToString());
            }
        }

        if (bagSlots.Count == 0)
        {
            for (int i = 0; i < BagSize; i++)
            {
                bagSlots.Add(false);
            }
        }

        return bagSlots;
    }

    private static string FullPath
    {
        get
        {
            if (string.IsNullOrEmpty(_fullPath))
            {
                var mod = ModManager.GetMod("WMMStashBackpack", true);
                _fullPath = mod.Path + "/SaveData/";
            }

            return _fullPath;
        }
    }

    private static string FullPathAndFilename
    {
        get
        {
            return FullPath + _filename;
        }
    }
}