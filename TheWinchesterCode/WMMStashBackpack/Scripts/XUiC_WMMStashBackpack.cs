﻿using HarmonyLib;
using System.Collections.Generic;

public class XUiC_WMMStashBackpack : XUiC_BackpackWindow
{
    private XUiController btnDump;
    private XUiController btnDumpMatching;
    private bool stashLocksInitiated = false;
    public static List<bool> BagLockedSlots = new List<bool>();

    public override void Init()
    {
        base.Init();

        btnDump = GetChildById("btnDump");
        btnDump.OnPress += BtnDump_OnPress;

        btnDumpMatching = GetChildById("btnDumpMatching");
        btnDumpMatching.OnPress += BtnDumpMatching_OnPress;
    }

    public override void OnOpen()
    {
        if (!stashLocksInitiated)
        {
            var childByType = GetChildByType<XUiC_ItemStackGrid>();
            var itemControllers = AccessTools.Field(typeof(XUiC_ItemStackGrid), "itemControllers").GetValue(childByType) as XUiController[];
            for (int i = 0; i < BagLockedSlots.Count; i++)
            {
                if(i >= itemControllers.Length)
                {
                    break;
                }

                var xuiC_ItemStack = (XUiC_ItemStack)itemControllers[i];
                if (xuiC_ItemStack != null)
                    xuiC_ItemStack.CustomAttributes.SetBool("IsDumpLocked", BagLockedSlots[i]);
            }

            if (StashBackpackCore.BagSize == 0)
            {
                Log.Out("OnOpen - BagSize : " + childByType.GetSlots().Length);
                StashBackpackCore.BagSize = childByType.GetSlots().Length;

                // Account for users changing the bagsize, maybe added a mod to increase bag size
                if(StashBackpackCore.BagSize > BagLockedSlots.Count)
                {
                    Log.Out("OnOpen A - " + BagLockedSlots.Count);
                    BagLockedSlots.AddRange(
                        new List<bool>(
                            new bool[StashBackpackCore.BagSize - BagLockedSlots.Count]
                            ));
                    Log.Out("OnOpen B - " + BagLockedSlots.Count);
                }
                else if(StashBackpackCore.BagSize < BagLockedSlots.Count)
                {
                    Log.Out("OnOpen C - " + BagLockedSlots.Count);
                    BagLockedSlots.RemoveRange(StashBackpackCore.BagSize, BagLockedSlots.Count - StashBackpackCore.BagSize);
                    Log.Out("OnOpen D - " + BagLockedSlots.Count);
                }
            }

            stashLocksInitiated = true;
        }

        base.OnOpen();
    }

    public void BtnDump_OnPress(XUiController _sender, int _mouseButton)
    {
        StashItems();
    }

    public void BtnDumpMatching_OnPress(XUiController _sender, int _mouseButton)
    {
        StashItems(true);
    }

    private void StashItems(bool matchItems = false)
    {
        var isVehicle = xui.vehicle != null && xui.vehicle.GetVehicle().HasStorage();
        var windowLooting = xui.GetWindow("windowLooting");
        if ((xui.playerUI.windowManager.IsWindowOpen("looting") && windowLooting != null) || isVehicle)
        {
            // Can only stash into player made containers
            var lootContainer = windowLooting.Controller.GetChildByType<XUiC_LootContainer>();
            var localTileEntity = AccessTools.Field(typeof(XUiC_LootContainer), "localTileEntity").GetValue(lootContainer) as TileEntityLootContainer;

            if (lootContainer == null)
                return;

            if (!isVehicle)
            {
                if (localTileEntity == null)
                    return;
            }

            var backpackSlots = xui.PlayerInventory.GetBackpackItemStacks();
            var childByType = GetChildByType<XUiC_ItemStackGrid>();
            var itemControllers = AccessTools.Field(typeof(XUiC_ItemStackGrid), "itemControllers").GetValue(childByType) as XUiController[];
            for (int i = 0; i < backpackSlots.Length; i++)
            {
                var xuiC_ItemStack = (XUiC_ItemStack)itemControllers[i];
                var itemStack = backpackSlots[i];

                if (xuiC_ItemStack != null && !xuiC_ItemStack.CustomAttributes.GetBool("IsDumpLocked"))
                {
                    // Quest items have an ItemClass.Id of zero so ignore these
                    if (itemStack != null && itemStack.itemValue != null && itemStack.itemValue.ItemClass != null && itemStack.itemValue.ItemClass.Id > 0)
                    {
                        if (matchItems && ((isVehicle && !VehicleHasItem(itemStack.itemValue, xui)) || (xui.lootContainer != null && !xui.lootContainer.HasItem(itemStack.itemValue))))
                            continue;

                        if (itemStack.itemValue.ItemClass.Stacknumber.Value <= 1)
                        {
                            if ((isVehicle && VehicleAddItem(itemStack, xui)) || XUiM_LootContainer.AddItem(itemStack, xui))
                                xui.PlayerInventory.Backpack.SetSlot(i, ItemStack.Empty.Clone(), true);
                        }
                        else
                        {
                            if ((isVehicle && VehicleAddItem(itemStack, xui)) || XUiM_LootContainer.AddItem(itemStack, xui))
                            {
                                xui.PlayerInventory.Backpack.SetSlot(i, ItemStack.Empty.Clone(), true);
                                continue;
                            }

                            if (itemStack.count > 0)
                                xui.PlayerInventory.Backpack.SetSlot(i, itemStack, true);
                            else
                                xui.PlayerInventory.Backpack.SetSlot(i, ItemStack.Empty.Clone(), true);
                        }
                    }
                }
            }
        }
    }

    public static bool VehicleAddItem(ItemStack _itemStack, XUi _xui)
    {
        if (_xui.vehicle.bag == null)
        {
            return false;
        }
        _xui.vehicle.bag.TryStackItem(0, _itemStack);
        return _itemStack.count > 0 && _xui.vehicle.bag.AddItem(_itemStack);
    }

    public bool VehicleHasItem(ItemValue _item, XUi _xui)
    {
        for (int i = 0; i < _xui.vehicle.bag.GetSlots().Length; i++)
        {
            if (_xui.vehicle.bag.GetSlots()[i].itemValue.ItemClass == _item.ItemClass)
            {
                return true;
            }
        }
        return false;
    }
}