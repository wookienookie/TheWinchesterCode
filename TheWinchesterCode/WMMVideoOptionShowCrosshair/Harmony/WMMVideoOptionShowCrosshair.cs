﻿using System;
using HarmonyLib;
using UnityEngine;
using System.Reflection;

public class WMMVideoOptionShowCrosshair
{
    public class WMMVideoOptionShowCrosshair_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(EntityPlayerLocal))]
    [HarmonyPatch("guiDrawCrosshair")]
    [HarmonyPatch(new Type[] { typeof(NGuiWdwInGameHUD), typeof(bool) })]
    public class PatchEntityPlayerLocalapplyChanges
    {
        static bool Prefix(EntityPlayerLocal __instance, NGuiWdwInGameHUD _guiInGame, bool bModalWindowOpen, ref NGUIWindowManager ___nguiWindowManager, ref LocalPlayerUI ___playerUI)
        {
            if (!_guiInGame.showCrosshair)
            {
                return false;
            }
            if (Event.current.type != EventType.Repaint)
            {
                return false;
            }
            if (__instance.IsDead())
            {
                return false;
            }
            if (__instance.AttachedToEntity != null)
            {
                return false;
            }
            ItemClass.EnumCrosshairType crosshairType = __instance.inventory.holdingItem.GetCrosshairType(__instance.inventory.holdingItemData);
            if (!bModalWindowOpen && __instance.inventory != null)
            {
                Vector2 crosshairPosition2D = __instance.GetCrosshairPosition2D();
                crosshairPosition2D.y = (float)Screen.height - crosshairPosition2D.y;
                switch (crosshairType)
                {
                    case ItemClass.EnumCrosshairType.Plus:
                        if (WMMCustomVideoOptions.VideoOptions.ShowCrosshair && Event.current.type == EventType.Repaint)
                        {
                            Color color = WMMCustomVideoOptions.VideoOptions.CrosshairColor;
                            GUI.color = new Color(color.r, color.g, color.b, _guiInGame.crosshairAlpha);
                            GUI.DrawTexture(new Rect(crosshairPosition2D.x - (float)(_guiInGame.CrosshairTexture.width / 2), crosshairPosition2D.y - (float)(_guiInGame.CrosshairTexture.height / 2), (float)_guiInGame.CrosshairTexture.width, (float)_guiInGame.CrosshairTexture.height), _guiInGame.CrosshairTexture);
                            GUI.color = color;
                            return false;
                        }
                        break;
                    case ItemClass.EnumCrosshairType.Crosshair:
                    case ItemClass.EnumCrosshairType.CrosshairOnAiming:
                        if (WMMCustomVideoOptions.VideoOptions.ShowCrosshair && (crosshairType != ItemClass.EnumCrosshairType.Crosshair || !__instance.AimingGun || ItemActionAttack.ShowDistanceDebugInfo))
                        {
                            __instance.GetCrosshairOpenArea();
                            float num = EffectManager.GetValue(PassiveEffects.SpreadDegreesHorizontal, __instance.inventory.holdingItemData.itemValue, 90f, __instance, null, default(FastTags), true, true, true, true, 1, true);
                            num *= 0.5f;
                            num *= (__instance.inventory.holdingItemData.actionData[0] as ItemActionRanged.ItemActionDataRanged).lastAccuracy;
                            num *= (float)Mathf.RoundToInt((float)Screen.width / __instance.cameraTransform.GetComponent<Camera>().fieldOfView);
                            float num2 = EffectManager.GetValue(PassiveEffects.SpreadDegreesVertical, __instance.inventory.holdingItemData.itemValue, 90f, __instance, null, default(FastTags), true, true, true, true, 1, true);
                            num2 *= 0.5f;
                            num2 *= (__instance.inventory.holdingItemData.actionData[0] as ItemActionRanged.ItemActionDataRanged).lastAccuracy;
                            num2 *= (float)Mathf.RoundToInt((float)Screen.width / __instance.cameraTransform.GetComponent<Camera>().fieldOfView);
                            int num3 = (int)crosshairPosition2D.x;
                            int num4 = (int)crosshairPosition2D.y;
                            int num5 = 18;
                            Color black = Color.black;
                            Color white = WMMCustomVideoOptions.VideoOptions.CrosshairColor;
                            black.a = CrosshairAlpha(_guiInGame) * __instance.weaponCrossHairAlpha;
                            white.a = CrosshairAlpha(_guiInGame) * __instance.weaponCrossHairAlpha;
                            GUIUtils.DrawLine(new Vector2((float)num3 - num, (float)(num4 + 1)), new Vector2((float)num3 - (num + (float)num5), (float)(num4 + 1)), black);
                            GUIUtils.DrawLine(new Vector2((float)num3 + num, (float)(num4 + 1)), new Vector2((float)num3 + num + (float)num5, (float)(num4 + 1)), black);
                            GUIUtils.DrawLine(new Vector2((float)(num3 + 1), (float)num4 - num2), new Vector2((float)(num3 + 1), (float)num4 - (num2 + (float)num5)), black);
                            GUIUtils.DrawLine(new Vector2((float)(num3 + 1), (float)num4 + num2), new Vector2((float)(num3 + 1), (float)num4 + num2 + (float)num5), black);
                            GUIUtils.DrawLine(new Vector2((float)num3 + num, (float)num4), new Vector2((float)num3 + num + (float)num5, (float)num4), white);
                            GUIUtils.DrawLine(new Vector2((float)num3, (float)num4 - num2), new Vector2((float)num3, (float)num4 - (num2 + (float)num5)), white);
                            GUIUtils.DrawLine(new Vector2((float)num3 - num, (float)num4), new Vector2((float)num3 - (num + (float)num5), (float)num4), white);
                            GUIUtils.DrawLine(new Vector2((float)num3, (float)num4 + num2), new Vector2((float)num3, (float)num4 + num2 + (float)num5), white);
                            GUIUtils.DrawLine(new Vector2((float)num3 - num, (float)(num4 - 1)), new Vector2((float)num3 - (num + (float)num5), (float)(num4 - 1)), black);
                            GUIUtils.DrawLine(new Vector2((float)num3 + num, (float)(num4 - 1)), new Vector2((float)num3 + num + (float)num5, (float)(num4 - 1)), black);
                            GUIUtils.DrawLine(new Vector2((float)(num3 - 1), (float)num4 - num2), new Vector2((float)(num3 - 1), (float)num4 - (num2 + (float)num5)), black);
                            GUIUtils.DrawLine(new Vector2((float)(num3 - 1), (float)num4 + num2), new Vector2((float)(num3 - 1), (float)num4 + num2 + (float)num5), black);
                            return false;
                        }
                        break;
                    case ItemClass.EnumCrosshairType.Damage:
                        if (Event.current.type == EventType.Repaint)
                        {
                            Color color2 = GUI.color;
                            if (___playerUI.xui.BackgroundGlobalOpacity < 1f)
                            {
                                float a = color2.a * ___playerUI.xui.BackgroundGlobalOpacity;
                                GUI.color = new Color(color2.r, color2.g, color2.b, a);
                            }
                            else
                            {
                                GUI.color = new Color(color2.r, color2.g, color2.b, CrosshairAlpha(_guiInGame));
                            }
                            GUI.DrawTexture(new Rect(crosshairPosition2D.x - 22f, crosshairPosition2D.y - 22f, 44f, 44f), _guiInGame.CrosshairDamage);
                            GUI.color = color2;
                            return false;
                        }
                        break;
                    case ItemClass.EnumCrosshairType.Upgrade:
                        if (Event.current.type == EventType.Repaint)
                        {
                            Color color3 = GUI.color;
                            if (___playerUI.xui.BackgroundGlobalOpacity < 1f)
                            {
                                float a2 = color3.a * ___playerUI.xui.BackgroundGlobalOpacity;
                                GUI.color = new Color(color3.r, color3.g, color3.b, a2);
                            }
                            else
                            {
                                GUI.color = new Color(color3.r, color3.g, color3.b, CrosshairAlpha(_guiInGame));
                            }
                            GUI.DrawTexture(new Rect(crosshairPosition2D.x - 22f, crosshairPosition2D.y - 22f, 44f, 44f), _guiInGame.CrosshairUpgrade);
                            GUI.color = color3;
                            return false;
                        }
                        break;
                    case ItemClass.EnumCrosshairType.Repair:
                        if (Event.current.type == EventType.Repaint)
                        {
                            Color color4 = GUI.color;
                            if (___playerUI.xui.BackgroundGlobalOpacity < 1f)
                            {
                                float a3 = color4.a * ___playerUI.xui.BackgroundGlobalOpacity;
                                GUI.color = new Color(color4.r, color4.g, color4.b, a3);
                            }
                            else
                            {
                                GUI.color = new Color(color4.r, color4.g, color4.b, CrosshairAlpha(_guiInGame));
                            }
                            GUI.DrawTexture(new Rect(crosshairPosition2D.x - 22f, crosshairPosition2D.y - 22f, 44f, 44f), _guiInGame.CrosshairRepair);
                            GUI.color = color4;
                            return false;
                        }
                        break;
                    case ItemClass.EnumCrosshairType.PowerSource:
                        if (Event.current.type == EventType.Repaint)
                        {
                            Color color5 = GUI.color;
                            if (___playerUI.xui.BackgroundGlobalOpacity < 1f)
                            {
                                float a4 = color5.a * ___playerUI.xui.BackgroundGlobalOpacity;
                                GUI.color = new Color(color5.r, color5.g, color5.b, a4);
                            }
                            else
                            {
                                GUI.color = new Color(color5.r, color5.g, color5.b, CrosshairAlpha(_guiInGame));
                            }
                            GUI.DrawTexture(new Rect(crosshairPosition2D.x - 22f, crosshairPosition2D.y - 22f, 44f, 44f), _guiInGame.CrosshairPowerSource);
                            GUI.color = color5;
                        }
                        break;
                    case ItemClass.EnumCrosshairType.Heal:
                        if (Event.current.type == EventType.Repaint)
                        {
                            Color color6 = GUI.color;
                            if (___playerUI.xui.BackgroundGlobalOpacity < 1f)
                            {
                                float a5 = color6.a * ___playerUI.xui.BackgroundGlobalOpacity;
                                GUI.color = new Color(color6.r, color6.g, color6.b, a5);
                            }
                            else
                            {
                                GUI.color = new Color(color6.r, color6.g, color6.b, CrosshairAlpha(_guiInGame));
                            }
                            GUI.DrawTexture(new Rect(crosshairPosition2D.x - 22f, crosshairPosition2D.y - 22f, 44f, 44f), _guiInGame.CrosshairRepair);
                            GUI.color = color6;
                            return false;
                        }
                        break;
                    case ItemClass.EnumCrosshairType.PowerItem:
                        if (Event.current.type == EventType.Repaint)
                        {
                            Color color7 = GUI.color;
                            if (___playerUI.xui.BackgroundGlobalOpacity < 1f)
                            {
                                float a6 = color7.a * ___playerUI.xui.BackgroundGlobalOpacity;
                                GUI.color = new Color(color7.r, color7.g, color7.b, a6);
                            }
                            else
                            {
                                GUI.color = new Color(color7.r, color7.g, color7.b, CrosshairAlpha(_guiInGame));
                            }
                            GUI.DrawTexture(new Rect(crosshairPosition2D.x - 22f, crosshairPosition2D.y - 22f, 44f, 44f), _guiInGame.CrosshairPowerItem);
                            GUI.color = color7;
                            return false;
                        }
                        break;
                    default:
                        return false;
                }
            }

            return false;
        }
    }

    private static float CrosshairAlpha(NGuiWdwInGameHUD _guiInGame)
    {
        return 1f; // Currently vanilla code just returns 1f so no need to call it atm
    }
}
