﻿using System;
using HarmonyLib;
using UnityEngine;
using System.Reflection;

public class WMMGameOptionHeadshotOnly
{
    //public class WMMGameOptionHeadshotOnly_Init : IModApi
    //{
    //    public void InitMod(Mod _modInstance)
    //    {
    //        Log.Out(" Loading Patch: " + this.GetType().ToString());

    //        // Reduce extra logging stuff
    //        Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
    //        Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

    //        var harmony = new HarmonyLib.Harmony(GetType().ToString());
    //        harmony.PatchAll(Assembly.GetExecutingAssembly());
    //    }
    //}

    [HarmonyPatch(typeof(EntityAlive))]
    [HarmonyPatch("DamageEntity")]
    [HarmonyPatch(new Type[] { typeof(DamageSource), typeof(int), typeof(bool), typeof(float) })]
    public class PatchEntityAliveDamageEntity
    {
        public static bool Prefix(EntityAlive __instance, ref DamageSource _damageSource, ref int _strength, bool _criticalHit, float _impulseScale)
        {
            if (WMMGameOptions.GetBool("CustomHeadShotOnly") && __instance is EntityZombie && !(__instance is EntityAnimalSnake))
            {
                if (_strength > 999)
                {                 
                    return true;
                }

                EnumBodyPartHit bodyPart = _damageSource.GetEntityDamageBodyPart(__instance);
                if (bodyPart == EnumBodyPartHit.Head)
                {
                       
                    _damageSource.DamageMultiplier = 1f;
                    _damageSource.DismemberChance += GetDismemberChance();
                }                  
                else
                {
                    _damageSource.DamageMultiplier = 0.00001f;
                    _strength = 0;
                }
            }

            return true;
        }

        private static float GetDismemberChance()
        {
            switch (GameStats.GetInt(EnumGameStats.GameDifficulty))
            {
                case 0:
                    return 0.2f;
                case 1:
                    return 0.16f;
                case 2:
                    return 0.12f;
                case 3:
                    return 0.08f;
                case 4:
                    return 0.04f;
                case 5:
                    return 0f;
            }

            return 0.8f;
        }
    }
}
