﻿using System;
using HarmonyLib;
using System.Reflection;
using UnityEngine;

public class WMMGameOptionBiomeRespawnDelay
{
    //public class WMMBiomeRespawnDelay_Init : IModApi
    //{
    //    public void InitMod(Mod _modInstance)
    //    {
    //        Log.Out(" Loading Patch: " + this.GetType().ToString());

    //        // Reduce extra logging stuff
    //        Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
    //        Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

    //        var harmony = new HarmonyLib.Harmony(GetType().ToString());
    //        harmony.PatchAll(Assembly.GetExecutingAssembly());
    //    }
    //}

    [HarmonyPatch(typeof(ChunkAreaBiomeSpawnData))]
    [HarmonyPatch("SetRespawnDelay")]
    [HarmonyPatch(new Type[] { typeof(string), typeof(ulong), typeof(WorldBiomes) })]
    public class PatchChunkAreaBiomeSpawnDataSetRespawnDelay
    {
        static bool Prefix(ChunkAreaBiomeSpawnData __instance, ref string _entityGroupName, ref ulong _currentWorldTime, ref WorldBiomes _worldBiomes, out int __state)
        {
            __state = 0;

            if (WMMGameOptions.GetInt("CustomBiomeRespawnDelay") == 1)
            {
                return true;
            }

            if (_entityGroupName == null)
            {
                return false;
            }
            BiomeDefinition biome = _worldBiomes.GetBiome(__instance.biomeId);
            if (biome == null)
            {
                return false;
            }
            BiomeSpawnEntityGroupList biomeSpawnEntityGroupList = BiomeSpawningClass.list[biome.m_sBiomeName];
            if (biomeSpawnEntityGroupList == null)
            {
                return false;
            }
            BiomeSpawnEntityGroupData biomeSpawnEntityGroupData = biomeSpawnEntityGroupList.Find(_entityGroupName);
            if (biomeSpawnEntityGroupData == null)
            {
                return false;
            }

            if( biomeSpawnEntityGroupData.entityGroupRefName.ToLower().Contains("bandit")
                || biomeSpawnEntityGroupData.entityGroupRefName.ToLower().Contains("wildgameforest")
                || biomeSpawnEntityGroupData.respawnDelayInWorldTime <= 7200)
            {
                return true;
            }

            __state = biomeSpawnEntityGroupData.respawnDelayInWorldTime;

            var value = 110 - (WMMGameOptions.GetInt("CustomBiomeRespawnDelay") * 10);
            float perc = (float)WMMGameOptions.Random.RandomRange(value, 100) / 100;
            __state = (int)(biomeSpawnEntityGroupData.respawnDelayInWorldTime * perc);

            return true;
        }

        static void Postfix(ChunkAreaBiomeSpawnData __instance, ref string _entityGroupName, ref ulong _currentWorldTime, ref WorldBiomes _worldBiomes, int __state)
        {
            if (WMMGameOptions.GetInt("CustomBiomeRespawnDelay") == 1
                || _entityGroupName == null
                || __state == 0)
            {
                return;
            }

            BiomeDefinition biome = _worldBiomes.GetBiome(__instance.biomeId);
            if (biome == null)
            {
                return;
            }
            BiomeSpawnEntityGroupList biomeSpawnEntityGroupList = BiomeSpawningClass.list[biome.m_sBiomeName];
            if (biomeSpawnEntityGroupList == null)
            {
                return;
            }
            BiomeSpawnEntityGroupData biomeSpawnEntityGroupData = biomeSpawnEntityGroupList.Find(_entityGroupName);
            if (biomeSpawnEntityGroupData == null)
            {
                return;
            }

            biomeSpawnEntityGroupData.respawnDelayInWorldTime = __state;
        }
    }
}

