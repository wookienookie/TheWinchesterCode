﻿using System;
using HarmonyLib;
using System.Reflection;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Linq;
using UnityEngine;
using System.Collections;

public class WMMGameOptionBiomeSpawnScaling
{
    private static bool GamestagesParsed = false;
    private static int LastLoggedMinute = 0;
    private static Dictionary<long, ulong> ChunkPOIs = new Dictionary<long, ulong>();
    private static Dictionary<long, int> ChunkZombieCount = new Dictionary<long, int>();

    //public class WMMBiomeSpawnScaling_Init : IModApi
    //{
    //    public void InitMod(Mod _modInstance)
    //    {
    //        Log.Out(" Loading Patch: " + this.GetType().ToString());

    //        // Reduce extra logging stuff
    //        Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
    //        Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

    //        var harmony = new HarmonyLib.Harmony(GetType().ToString());
    //        harmony.PatchAll(Assembly.GetExecutingAssembly());
    //    }
    //}

    //private static void LoadBiomeGamestages()
    //{
    //    if (!GamestagesParsed)
    //    {
    //        foreach (var key in BiomeSpawningClass.list.Dict.Keys)
    //        {
    //            var biomeSpawnEntityGroupList = BiomeSpawningClass.list[key];
    //            foreach (var biomeSpawnEntityGroupData in biomeSpawnEntityGroupList.list)
    //            {
    //                if (!WMMCore.BiomeSpawnGamestages.ContainsKey(biomeSpawnEntityGroupData.entityGroupRefName))
    //                {
    //                    var gs = 1;

    //                    if (biomeSpawnEntityGroupData.entityGroupRefName.Contains("-")
    //                    && biomeSpawnEntityGroupData.entityGroupRefName.Split('-').Length > 1
    //                    && int.TryParse(biomeSpawnEntityGroupData.entityGroupRefName.Split('-')[1], out gs))
    //                    { }

    //                    WMMCore.BiomeSpawnGamestages.Add(biomeSpawnEntityGroupData.entityGroupRefName, gs);
    //                }
    //            }
    //        }

    //        GamestagesParsed = true;
    //    }
    //}

    [HarmonyPatch(typeof(ChunkAreaBiomeSpawnData))]
    [HarmonyPatch("IsSpawnNeeded")]
    [HarmonyPatch(new Type[] { typeof(WorldBiomes), typeof(ulong) })]
    public class PatchChunkAreaBiomeSpawnDataIsSpawnNeeded
    {
        static bool Prefix(ChunkAreaBiomeSpawnData __instance, ref bool __result, ref WorldBiomes _worldBiomes, ref ulong _worldTime)
        {
            if (DateTime.Now.Second == 0 && LastLoggedMinute != DateTime.Now.Minute)
            {
                LastLoggedMinute = DateTime.Now.Minute;

                var biomeEnemyCount = 0;
                var num = 0;
                for (int i = GameManager.Instance.World.Entities.list.Count - 1; i >= 0; i--)
                {
                    Entity entity = GameManager.Instance.World.Entities.list[i];
                    if (entity.IsDead())
                        continue;

                    EntityAlive entityAlive = null;
                    if (entity is EntityAlive)
                    {
                        entityAlive = (EntityAlive)entity;
                    }

                    if (!entity.IsDead() && entityAlive != null && entityAlive.GetSpawnerSource() == EnumSpawnerSource.Biome)
                    {
                        if (EntityClass.list[entity.entityClass] != null && EntityClass.list[entity.entityClass].bIsEnemyEntity)
                            biomeEnemyCount++;
                    }
                }

                Log.Out("Number of enemies in game is " + GameStats.GetInt(EnumGameStats.EnemyCount) + " out of a maximum " + GetMaxZombies() + ". Biome enemy count = " + biomeEnemyCount);
            }

            //LoadBiomeGamestages();

            BiomeDefinition biome = _worldBiomes.GetBiome(__instance.biomeId);
            if (biome == null)
            {
                __result = false;
                return false;
            }
            BiomeSpawnEntityGroupList biomeSpawnEntityGroupList = BiomeSpawningClass.list[biome.m_sBiomeName];
            if (biomeSpawnEntityGroupList == null)
            {
                __result = false;
                return false;
            }

            if (GameStats.GetInt(EnumGameStats.EnemyCount) >= (GetMaxZombies() * 0.9))
            {
                __result = false;
                return false;
            }

            if (ChunkPOIs == null)
                ChunkPOIs = new Dictionary<long, ulong>();

            if (!ChunkPOIs.Keys.Contains(__instance.chunk.Key))
            {
                var POIs = GetAllPrefabFromWorldPosInside(__instance.chunk);
                ChunkPOIs.Add(__instance.chunk.Key, (ulong)POIs.Count());
            }

            //var list = GetBiomeSpawnEntityGroupData(biomeSpawnEntityGroupList.list, __instance.chunk, out int partyLevel);
            var list = biomeSpawnEntityGroupList.list;
            var entitesSpawned = AccessTools.Field(typeof(ChunkAreaBiomeSpawnData), "entitesSpawned").GetValue(__instance) as IDictionary;
            if (entitesSpawned != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    string key = list[i].entityGroupRefName + "_" + list[i].daytime.ToStringCached<EDaytime>();
                    var checkDaytime = false;

                    object value = entitesSpawned[key];
                    if (value == null) // emulates the __instance.entitesSpawned.ContainsKey(key) check, i.e. the value is not in the dictionary
                        checkDaytime = true;
                    else
                    {
                        // This means the value is in the dictionary so get the specific fields
                        var count = value.GetType().GetField("count").GetValue(value) as int?;
                        var delayWorldTime = value.GetType().GetField("delayWorldTime").GetValue(value) as ulong?;
                        if ((count.HasValue && count < GetPOISpawnCount(list[i].maxCount, ChunkPOIs[__instance.chunk.Key], __instance.chunk.Key, key))
                            || (delayWorldTime.HasValue && delayWorldTime <= _worldTime))
                        {
                            checkDaytime = true;
                        }
                    }

                    if (checkDaytime)
                    {
                        EDaytime edaytime = (!GameManager.Instance.World.IsDaytime()) ? EDaytime.Night : EDaytime.Day;
                        if (list[i].daytime == EDaytime.Any || list[i].daytime == edaytime)
                        {
                            __result = true;
                            return false;
                        }
                    }
                }
            }
            __result = false;
            return false;
        }
    }

    [HarmonyPatch(typeof(SpawnManagerBiomes))]
    [HarmonyPatch("SpawnUpdate")]
    [HarmonyPatch(new Type[] { typeof(string), typeof(bool), typeof(ChunkAreaBiomeSpawnData) })]
    public class PatchSpawnManagerBiomesSpawnUpdate
    {
        static bool Prefix(SpawnManagerBiomes __instance, 
            ref string _spawnerName, 
            ref bool _isSpawnEnemy, 
            ref ChunkAreaBiomeSpawnData _chunkBiomeSpawnData, 
            ref World ___world, 
            ref List<PrefabInstance> ___spawnPIs, 
            ref List<Entity> ___spawnNearList, 
            ref int ___lastClassId)
        {
            if (_chunkBiomeSpawnData != null)
            {
                if (_isSpawnEnemy)
                {
                    if (GameStats.GetInt(EnumGameStats.EnemyCount) >= GamePrefs.GetInt(EnumGamePrefs.MaxSpawnedZombies))
                    {
                        _isSpawnEnemy = false;
                    }
                    else if (___world.aiDirector.BloodMoonComponent.BloodMoonActive)
                    {
                        _isSpawnEnemy = false;
                    }
                }
                if (_isSpawnEnemy || GameStats.GetInt(EnumGameStats.AnimalCount) < GamePrefs.GetInt(EnumGamePrefs.MaxSpawnedAnimals))
                {
                    bool flag = false;
                    List<EntityPlayer> players = ___world.GetPlayers();
                    for (int i = 0; i < players.Count; i++)
                    {
                        EntityPlayer entityPlayer = players[i];
                        if (entityPlayer.Spawned && new Rect(entityPlayer.position.x - 40f, entityPlayer.position.z - 40f, 80f, 80f).Overlaps(_chunkBiomeSpawnData.area))
                        {
                            flag = true;
                            break;
                        }
                    }
                    if (flag)
                    {
                        int minDistance = _isSpawnEnemy ? 28 : 48;
                        int uNUSED_maxDistance = _isSpawnEnemy ? 54 : 70;
                        if (___world.GetRandomSpawnPositionInAreaMinMaxToPlayers(_chunkBiomeSpawnData.area, minDistance, uNUSED_maxDistance, true, out Vector3 _position))
                        {
                            BiomeDefinition biome = ___world.Biomes.GetBiome(_chunkBiomeSpawnData.biomeId);
                            if (biome != null)
                            {
                                BiomeSpawnEntityGroupList biomeSpawnEntityGroupList = BiomeSpawningClass.list[biome.m_sBiomeName];
                                if (biomeSpawnEntityGroupList != null)
                                {
                                    //var list = GetBiomeSpawnEntityGroupData(biomeSpawnEntityGroupList.list, _chunkBiomeSpawnData.chunk, out int partyLevel);
                                    var list = biomeSpawnEntityGroupList.list;
                                    EDaytime eDaytime = ___world.IsDaytime() ? EDaytime.Day : EDaytime.Night;
                                    GameRandom gameRandom = ___world.GetGameRandom();
                                    if (!_chunkBiomeSpawnData.checkedPOITags)
                                    {
                                        _chunkBiomeSpawnData.checkedPOITags = true;
                                        POITags pOITags = POITags.none;
                                        Vector3i worldPos = _chunkBiomeSpawnData.chunk.GetWorldPos();
                                        ___world.GetPOIsAtXZ(worldPos.x + 16, worldPos.x + 80 - 16, worldPos.z + 16, worldPos.z + 80 - 16, ___spawnPIs);
                                        for (int j = 0; j < ___spawnPIs.Count; j++)
                                        {
                                            PrefabInstance prefabInstance = ___spawnPIs[j];
                                            pOITags |= prefabInstance.prefab.Tags;
                                        }
                                        _chunkBiomeSpawnData.poiTags = pOITags;
                                        bool isEmpty = pOITags.IsEmpty;
                                        for (int k = 0; k < list.Count; k++)
                                        {
                                            BiomeSpawnEntityGroupData biomeSpawnEntityGroupData = list[k];
                                            if ((biomeSpawnEntityGroupData.POITags.IsEmpty || biomeSpawnEntityGroupData.POITags.Test_AnySet(pOITags)) && (isEmpty || biomeSpawnEntityGroupData.noPOITags.IsEmpty || !biomeSpawnEntityGroupData.noPOITags.Test_AnySet(pOITags)))
                                            {
                                                _chunkBiomeSpawnData.groupsEnabledFlags |= 1 << k;
                                            }
                                        }
                                    }
                                    string entityGroupName = null;
                                    int num = -1;
                                    int num2 = gameRandom.RandomRange(list.Count);
                                    int num3 = Utils.FastMin(5, list.Count);
                                    for (int l = 0; l < num3; l++, num2 = (num2 + 1) % list.Count)
                                    {
                                        BiomeSpawnEntityGroupData biomeSpawnEntityGroupData2 = list[num2];
                                        if ((_chunkBiomeSpawnData.groupsEnabledFlags & (1 << num2)) != 0 && (biomeSpawnEntityGroupData2.daytime == EDaytime.Any || biomeSpawnEntityGroupData2.daytime == eDaytime))
                                        {
                                            bool flag2 = EntityGroups.IsEnemyGroup(biomeSpawnEntityGroupData2.entityGroupRefName);
                                            if (!flag2 || _isSpawnEnemy)
                                            {
                                                int num4 = biomeSpawnEntityGroupData2.maxCount;
                                                if (flag2)
                                                {
                                                    num4 = EntitySpawner.ModifySpawnCountByGameDifficulty(num4);
                                                }
                                                num4 = GetPOISpawnCount(num4, ChunkPOIs[_chunkBiomeSpawnData.chunk.Key], _chunkBiomeSpawnData.chunk.Key, biomeSpawnEntityGroupData2.entityGroupRefName, true);
                                                entityGroupName = biomeSpawnEntityGroupData2.entityGroupRefName + "_" + biomeSpawnEntityGroupData2.daytime.ToStringCached();
                                                ulong respawnDelayWorldTime = _chunkBiomeSpawnData.GetRespawnDelayWorldTime(entityGroupName);
                                                if (respawnDelayWorldTime != 0)
                                                {
                                                    if (___world.worldTime < respawnDelayWorldTime)
                                                    {
                                                        continue;
                                                    }
                                                    _chunkBiomeSpawnData.ClearRespawn(entityGroupName);
                                                }
                                                if (_chunkBiomeSpawnData.GetEntitiesSpawned(entityGroupName) < num4)
                                                {
                                                    num = num2;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (num >= 0)
                                    {
                                        Bounds bb = new Bounds(_position, new Vector3(4f, 2.5f, 4f));
                                        ___world.GetEntitiesInBounds(typeof(Entity), bb, ___spawnNearList);
                                        int count = ___spawnNearList.Count;
                                        ___spawnNearList.Clear();
                                        if (count <= 0)
                                        {
                                            BiomeSpawnEntityGroupData biomeSpawnEntityGroupData3 = list[num];
                                            int randomFromGroup = EntityGroups.GetRandomFromGroup(biomeSpawnEntityGroupData3.entityGroupRefName, ref ___lastClassId, null);
                                            if (randomFromGroup == 0)
                                            {
                                                _chunkBiomeSpawnData.SetRespawnDelay(entityGroupName, ___world.worldTime, ___world.Biomes);
                                            }
                                            else
                                            {
                                                _chunkBiomeSpawnData.IncEntitiesSpawned(entityGroupName);
                                                Entity entity = EntityFactory.CreateEntity(randomFromGroup, _position);
                                                entity.SetSpawnerSource(EnumSpawnerSource.Biome, _chunkBiomeSpawnData.chunk.Key, entityGroupName);
                                                ___world.SpawnEntityInWorld(entity);
                                                float spawnDeadChance = biomeSpawnEntityGroupData3.spawnDeadChance;
                                                if (spawnDeadChance > 0f && gameRandom.RandomFloat < spawnDeadChance)
                                                {
                                                    entity.Kill(DamageResponse.New(true));
                                                }
                                                ___world.DebugAddSpawnedEntity(entity);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return false;
        }
    }

    public static int GetGamestageFromBounds(int x, int y, int z, string source, bool log = false)
    {
        // Get Players gamestage
        return GetGamestageFromBounds((float)x, (float)y, (float)z, source, log);
    }

    public static int GetGamestageFromBounds(float x, float y, float z, string source, bool log = false)
    {
        //if (log)
        //    Log.Out("BiomeSpawning ---------");

        // Get Players gamestage
        var chunkPosition = new Vector3(x, y, z);
        var bounds = new Bounds(new Vector3(chunkPosition.x, chunkPosition.y, chunkPosition.z), new Vector3(200f, 200f, 200f));
        List<Entity> playersInBounds = GameManager.Instance.World.GetEntitiesInBounds(typeof(EntityPlayer), bounds, new List<Entity>());
        List<int> partyLevelList = new List<int>();
        foreach (var entityBound in playersInBounds)
        {
            var player = entityBound as EntityPlayer;
            partyLevelList.Add(player.gameStage);

            //if (log)
            //    Log.Out("BiomeSpawning Player in bound : " + player.entityId + " : GS = " + player.gameStage + " : source = " + source);
        }

        return Mathf.Max(GameStageDefinition.CalcPartyLevel(partyLevelList), 1);
    }

    public static List<PrefabInstance> GetAllPrefabFromWorldPosInside(Chunk _chunk)
    {
        var allPrefabs = GameManager.Instance.World.ChunkClusters[0].ChunkProvider.GetDynamicPrefabDecorator().GetDynamicPrefabs();
        var prefabs = new List<PrefabInstance>();

        for (int i = 0; i < allPrefabs.Count; i++)
        {
            PrefabInstance prefabInstance = allPrefabs[i];
            if (Overlaps(prefabInstance, _chunk, 75f))
            {
                prefabs.Add(allPrefabs[i]);
            }
        }

        return prefabs;
    }

    public static bool Overlaps(PrefabInstance prefabInstance, Chunk _chunk, float _expandBounds = 0f)
    {
        Bounds aabb = _chunk.GetAABB();
        aabb.Expand(_expandBounds);
        Vector3 max = aabb.max;
        Vector3 min = aabb.min;
        Bounds aabb2 = prefabInstance.GetAABB();
        Vector3 max2 = aabb2.max;
        Vector3 min2 = aabb2.min;

        return min.x <= max2.x && max.x >= min2.x && min.y <= max2.y && max.y >= min2.y && min.z <= max2.z && max.z >= min2.z;
    }

    public static int GetPOISpawnCount(int maxCount, ulong poiCount, long chunkKey, string entityGroupRefName, bool log = false)
    {
        //if(entityGroupRefName.ToLower().Contains("wildgameforest"))
        //{
        //    return maxCount;
        //}

        if(ChunkZombieCount.ContainsKey(chunkKey))
        {
            return ChunkZombieCount[chunkKey];
        }

        var multiplier = WMMGameOptions.Random.RandomRange(1, WMMGameOptions.GetInt("CustomCityZombieMultiplier") + 1);
        Log.Out("BiomeSpawnMultiplier : " + multiplier);
        var count = maxCount + ((int)poiCount * multiplier);
        var enemyCount = GameStats.GetInt(EnumGameStats.EnemyCount);
        var maxCountScalingPercent = 0f;
        if (enemyCount > 0)
            maxCountScalingPercent = (enemyCount >= GetMaxZombies()) ? 1f : ((float)enemyCount / (float)GetMaxZombies());

        // Maximum per chunk is 1/6th of max zombies. This is then scaled on percentage of max zombies already spawned in. 
        // So if max zombies is 120 thats a max of 20 per chunk. If 60 zombies are already spawned in then max is 50% of 20 = max per chunk of 10 zombies.
        count = Mathf.Min(count, (int)((GetMaxZombies() / 6) * (1 - maxCountScalingPercent)));

        ChunkZombieCount[chunkKey] = count;

        return count;
    }

    //public static List<BiomeSpawnEntityGroupData> GetBiomeSpawnEntityGroupData(List<BiomeSpawnEntityGroupData> biomeSpawnEntityGroupList, Chunk chunk, out int partyLevel)
    //{
    //    // Get Players gamestage
    //    partyLevel = GetGamestageFromBounds(chunk.X * 16, chunk.Y * 16, chunk.Z * 16, chunk.Key.ToString());

    //    // Get list of biomeSpawnEntityGroup
    //    int matchingGameStage = 0;
    //    List<BiomeSpawnEntityGroupData> list = new List<BiomeSpawnEntityGroupData>();
    //    foreach (var x in biomeSpawnEntityGroupList.OrderByDescending(x => WMMCore.BiomeSpawnGamestages[x.entityGroupRefName]).AsEnumerable())
    //    {
    //        var gs = WMMCore.BiomeSpawnGamestages[x.entityGroupRefName];

    //        //Log.Out("SpawnLogic-" + guid + " : " + x.entityGroupRefName + " : " + x.gameStage);
    //        // Automatically include spawns for animals whatever the GS
    //        if (x.entityGroupRefName.ToLower().Contains("wildgameforest"))
    //            list.Add(x);

    //        // Finding matching gamestage
    //        if (matchingGameStage == 0 && gs <= partyLevel)
    //            matchingGameStage = gs;

    //        // Animals already included so they are excluded
    //        if (matchingGameStage > 0 && !x.entityGroupRefName.ToLower().Contains("wildgameforest"))
    //        {
    //            if (gs != matchingGameStage) continue;
    //            list.Add(x);
    //        }
    //    }

    //    return list;
    //}

    private static int GetMaxZombies()
    {
        return GamePrefs.GetInt(EnumGamePrefs.MaxSpawnedZombies);
    }
}

