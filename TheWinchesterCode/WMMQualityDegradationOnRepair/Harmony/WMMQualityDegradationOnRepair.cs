﻿//using System;
//using HarmonyLib;
//using System.Reflection;
//using UnityEngine;
//using Audio;
//using System.Collections.Generic;

//public class WMMQualityDegradationOnRepair
//{
//    public class WMMQualityDegradationOnRepair_Init : IModApi
//    {
//        public void InitMod(Mod _modInstance)
//        {
//            Log.Out(" Loading Patch: " + this.GetType().ToString());

//            // Reduce extra logging stuff
//            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
//            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

//            var harmony = new HarmonyLib.Harmony(GetType().ToString());
//            harmony.PatchAll(Assembly.GetExecutingAssembly());
//        }
//    }

//    [HarmonyPatch(typeof(XUiC_RecipeStack))]
//    [HarmonyPatch("outputStack")]
//    class PatchXUiC_RecipeStackoutputStack
//    {
//        static bool Prefix(XUiC_RecipeStack __instance,
//            ref bool __result,
//            ref Recipe ___recipe,
//            ref ItemValue ___originalItem,
//            ref ItemValue ___outputItemValue,
//            ref int ___outputQuality,
//            ref int ___startingEntityId,
//            ref int ___amountToRepair,
//            ref bool ___playSound,
//            ref bool ___isInventoryFull)
//        {
//            if (___recipe == null)
//            {
//                __result = false;
//                return false;
//            }
//            EntityPlayerLocal entityPlayer = __instance.xui.playerUI.entityPlayer;
//            if (entityPlayer == null)
//            {
//                __result = false;
//                return false;
//            }
//            ItemValue returnedMod = null;
//            if (___originalItem == null || ___originalItem.Equals(ItemValue.None))
//            {
//                ___outputItemValue = new ItemValue(___recipe.itemValueType, ___outputQuality, ___outputQuality, false, null, 1f);
//                ItemClass itemClass = ___outputItemValue.ItemClass;
//                if (___outputItemValue == null)
//                {
//                    __result = false;
//                    return false;
//                }
//                if (itemClass == null)
//                {
//                    __result = false;
//                    return false;
//                }
//                if (entityPlayer.entityId == ___startingEntityId)
//                {
//                    var giveExpMethod = AccessTools.Method(typeof(XUiC_RecipeStack), "giveExp");
//                    giveExpMethod.Invoke(__instance, new object[] { ___outputItemValue, itemClass });
//                }
//                if (___recipe.GetName().Equals("meleeToolStoneAxe"))
//                {
//                    Platform.PlatformManager.NativePlatform.AchievementManager.SetAchievementStat(Platform.EnumAchievementDataStat.StoneAxeCrafted, 1);
//                }
//                else if (___recipe.GetName().Equals("woodFrameBlockVariantHelper"))
//                {
//                    Platform.PlatformManager.NativePlatform.AchievementManager.SetAchievementStat(Platform.EnumAchievementDataStat.WoodFrameCrafted, 1);
//                }
//            }
//            else if (___amountToRepair > 0)
//            {
//                ItemValue itemValue2 = ___originalItem.Clone();
//                itemValue2.UseTimes -= ___amountToRepair;
//                ItemClass itemClass2 = itemValue2.ItemClass;
//                if (itemValue2.UseTimes < 0)
//                {
//                    itemValue2.UseTimes = 0;
//                }
//                ___outputItemValue = itemValue2.Clone();
              
//                float num = 100f;
//                if (entityPlayer.entityId == ___startingEntityId)
//                {
//                    num = QualityDegradationHelper.CalculateQualityLoss(itemValue2, entityPlayer);
//                }

//                var newQuality = ___outputItemValue.Quality - (int)num;
//                ___outputItemValue.Quality = newQuality;

//                var tempItem = new ItemValue(___outputItemValue.type, ___outputItemValue.Quality, ___outputItemValue.Quality, false, null, 1f);
//                if (tempItem.Modifications.Length < ___outputItemValue.Modifications.Length)
//                {
//                    if (___outputItemValue.Modifications[___outputItemValue.Modifications.Length - 1] != null)
//                    {
//                        returnedMod = ___outputItemValue.Modifications[___outputItemValue.Modifications.Length - 1].Clone();
//                    }
//                    ItemValue[] modifications = ___outputItemValue.Modifications;
//                    Array.Resize(ref modifications, ___outputItemValue.Modifications.Length - 1);
//                    ___outputItemValue.Modifications = modifications;
//                }
                
//                QuestEventManager.Current.RepairedItem(___outputItemValue);
//                ___amountToRepair = 0;
//            }

//            var windowGroup = AccessTools.Field(typeof(XUiController), "windowGroup").GetValue(__instance) as XUiWindowGroup;
//            XUiC_WorkstationOutputGrid childByType = windowGroup.Controller.GetChildByType<XUiC_WorkstationOutputGrid>();
//            if (childByType != null && (___originalItem == null || ___originalItem.Equals(ItemValue.None)))
//            {
//                ItemStack itemStack = new ItemStack(___outputItemValue, ___recipe.count);
//                ItemStack[] slots = childByType.GetSlots();
//                bool flag = false;
//                for (int i = 0; i < slots.Length; i++)
//                {
//                    if (slots[i].CanStackWith(itemStack))
//                    {
//                        slots[i].count += ___recipe.count;
//                        flag = true;
//                        break;
//                    }
//                }
//                if (!flag)
//                {
//                    for (int j = 0; j < slots.Length; j++)
//                    {
//                        if (slots[j].IsEmpty())
//                        {
//                            slots[j] = itemStack;
//                            flag = true;
//                            break;
//                        }
//                    }
//                }
//                if (flag)
//                {
//                    childByType.SetSlots(slots);
//                    childByType.UpdateData(slots);
//                    childByType.IsDirty = true;
//                    QuestEventManager.Current.CraftedItem(itemStack);
//                    if (___playSound)
//                    {
//                        if (___recipe.craftingArea != null)
//                        {
//                            WorkstationData workstationData = CraftingManager.GetWorkstationData(___recipe.craftingArea);
//                            if (workstationData != null)
//                            {
//                                Manager.PlayInsidePlayerHead(workstationData.CraftCompleteSound, -1, 0f, false);
//                            }
//                        }
//                        else
//                        {
//                            Manager.PlayInsidePlayerHead("craft_complete_item", -1, 0f, false);
//                        }
//                    }
//                }
//                else
//                {
//                    var AddItemToInventoryMethod = AccessTools.Method(typeof(XUiC_RecipeStack), "AddItemToInventory");
//                    var addItemToInventory = AddItemToInventoryMethod.Invoke(__instance, new object[] { }) as bool?;

//                    if (addItemToInventory.HasValue && !addItemToInventory.Value)
//                    {
//                        ___isInventoryFull = true;
//                        string text = "No room in workstation output, crafting has been halted until space is cleared.";
//                        if (Localization.Exists("wrnWorkstationOutputFull"))
//                        {
//                            text = Localization.Get("wrnWorkstationOutputFull");
//                        }
//                        GameManager.ShowTooltip(entityPlayer, text);
//                        Manager.PlayInsidePlayerHead("ui_denied", -1, 0f, false);
//                        __result = false;
//                        return false;
//                    }
//                }
//            }
//            else
//            {
//                if (!__instance.xui.dragAndDrop.CurrentStack.IsEmpty() && __instance.xui.dragAndDrop.CurrentStack.itemValue.ItemClass is ItemClassQuest)
//                {
//                    __result = false;
//                    return false;
//                }
//                ItemStack itemStack2 = new ItemStack(___outputItemValue, ___recipe.count);
//                if (!__instance.xui.PlayerInventory.AddItemNoPartial(itemStack2, false))
//                {
//                    if (itemStack2.count != ___recipe.count)
//                    {
//                        __instance.xui.PlayerInventory.DropItem(itemStack2);
//                        QuestEventManager.Current.CraftedItem(itemStack2);
//                        __result = true;
//                        return false;
//                    }
//                    ___isInventoryFull = true;
//                    string text2 = "No room in inventory, crafting has been halted until space is cleared.";
//                    if (Localization.Exists("wrnInventoryFull"))
//                    {
//                        text2 = Localization.Get("wrnInventoryFull");
//                    }
//                    GameManager.ShowTooltip(entityPlayer, text2);
//                    Manager.PlayInsidePlayerHead("ui_denied", -1, 0f, false);
//                    __result = false;
//                    return false;
//                }
//                else
//                {
//                    if (___originalItem != null && !___originalItem.IsEmpty())
//                    {
//                        if (___recipe.ingredients.Count > 0)
//                        {
//                            QuestEventManager.Current.ScrappedItem(___recipe.ingredients[0]);
//                        }
//                    }
//                    else
//                    {
//                        itemStack2.count = ___recipe.count - itemStack2.count;
//                        QuestEventManager.Current.CraftedItem(itemStack2);
//                    }
//                    if (___playSound)
//                    {
//                        Manager.PlayInsidePlayerHead("craft_complete_item", -1, 0f, false);
//                    }
//                }
//            }
//            if (!___isInventoryFull)
//            {
//                ___originalItem = ItemValue.None.Clone();
//            }
//            if (returnedMod != null)
//            {
//                ItemStack itemStack3 = new ItemStack(returnedMod, 1);
//                if (!__instance.xui.PlayerInventory.AddItemNoPartial(itemStack3, false))
//                {
//                    __instance.xui.PlayerInventory.DropItem(itemStack3);
//                }
//            }

//            __result = true;
//            return false;
//        }
//    }

//    // Disable the Repair button if the item will go below 1 Quality
//    [HarmonyPatch(typeof(ItemActionEntryRepair))]
//    [HarmonyPatch("RefreshEnabled")]
//    class PatchItemActionEntryRepairRefreshEnabled
//    {
//        static bool Prefix(ItemActionEntryRepair __instance)
//        {
//            var xUiC_ItemStack = (XUiC_ItemStack)__instance.ItemController;
//            var itemStack = xUiC_ItemStack.ItemStack;
//            if (itemStack.IsEmpty() || xUiC_ItemStack.StackLock)
//            {
//                return true;
//            }

//            var itemValue = itemStack.itemValue;
//            var entityPlayer = __instance.ItemController.xui.playerUI.entityPlayer;
//            var num = QualityDegradationHelper.CalculateQualityLoss(itemValue, entityPlayer);
//            var newQuality = itemValue.Quality - (int)num;

//            if (newQuality < 1)
//            {
//                __instance.Enabled = false;
//                xUiC_ItemStack.CustomAttributes.Set("RepairDisabled", true);

//                return false;
//            }

//            xUiC_ItemStack.CustomAttributes.Set("RepairDisabled", false);

//            return true;
//        }
//    }

//    [HarmonyPatch(typeof(ItemActionEntryRepair))]
//    [HarmonyPatch("OnDisabledActivate")]
//    class PatchItemActionEntryRepairOnDisabledActivate
//    {
//        static bool Prefix(ItemActionEntryRepair __instance)
//        {
//            var xUiC_ItemStack = (XUiC_ItemStack)__instance.ItemController;

//            if(xUiC_ItemStack.CustomAttributes.GetBool("RepairDisabled"))
//            {
//                GameManager.ShowTooltip(__instance.ItemController.xui.playerUI.entityPlayer, "You do not have the required skill to repair this item. Increasing Action Skills may allow this item to be repaired.", "ui_denied");
//            }

//            return true;
//        }
//    }

//    [HarmonyPatch(typeof(XUiC_ItemInfoWindow))]
//    [HarmonyPatch("GetBindingValue")]
//    [HarmonyPatch(new Type[] { typeof(string), typeof(string) }, new ArgumentType[] { ArgumentType.Ref, ArgumentType.Normal })]
//    class PatchXUiC_ItemInfoWindowGetBindingValue
//    {
//        static void Postfix(XUiC_ItemInfoWindow __instance, ref bool __result, ref string value, ref string bindingName, ref ItemClass ___itemClass, ref ItemStack ___itemStack)
//        {
//            if (bindingName == "itemdescription")
//            {
//                value = "";
               
//                if (___itemClass != null)
//                {
//                    if (___itemClass.IsBlock())
//                    {
//                        string descriptionKey = Block.list[___itemClass.Id].DescriptionKey;
//                        if (Localization.Exists(descriptionKey))
//                        {
//                            value = Localization.Get(descriptionKey);
//                        }
//                    }
//                    else
//                    {
//                        string descriptionKey2 = ___itemClass.DescriptionKey;
//                        if (Localization.Exists(descriptionKey2))
//                        {
//                            value = Localization.Get(descriptionKey2);
//                        }
//                    }

//                    if (___itemStack != null && ___itemStack.itemValue != null && ___itemStack.itemValue.HasQuality)
//                    {
//                        value += "\nRepaired quality will be : ";
//                        value += ___itemStack.itemValue.Quality - QualityDegradationHelper.CalculateQualityLoss(___itemStack.itemValue, __instance.xui.playerUI.entityPlayer);
//                    }
//                }
//            }
//        }
//    }
//}

//static class QualityDegradationHelper
//{
//    public static float CalculateQualityLoss(ItemValue itemValue, EntityPlayerLocal entityPlayer)
//    {
//        Dictionary<string, string> skills = new Dictionary<string, string>()
//        {
//            { "constructionTool","AS_ConstructionTools_Lvl" },
//            { "miningTool","AS_MiningTools_Lvl" },
//            { "perkPummelPete","AS_Blunt_Lvl" },
//            { "perkSkullCrusher","AS_Blunt_Lvl" },
//            { "perkDeepCuts","AS_Bladed_Lvl" },
//            { "perkElectrocutioner","AS_Electric_Lvl" },
//            { "perkTurrets","AS_Turret_Lvl" },
//            { "perkJavelinMaster","AS_Javelin_Lvl" },
//            { "perkBrawler","AS_Brawler_Lvl" },
//            { "perkArchery","AS_Archery_Lvl" },
//            { "perkGunslinger","AS_Pistols_Lvl" },
//            { "perkBoomstick","AS_Shotguns_Lvl" },
//            { "perkDeadEye","AS_Rifles_Lvl" },
//            { "lightArmor","AS_ConstructionTools_Lvl" },
//            { "heavyArmor","AS_ConstructionTools_Lvl" },
//            { "perkMachineGunner","AS_Automatics_Lvl" }
//        };

//        float num = 100f;

//        foreach (var key in skills.Keys)
//        {
//            if (itemValue.ItemClass.HasAnyTags(FastTags.Parse(key)) && entityPlayer.Buffs.GetCustomVar(skills[key]) >= 20)
//            {
//                num = 120 - entityPlayer.Buffs.GetCustomVar(skills[key]);

//                break;
//            }
//        }

//        return num;
//    }
//}
