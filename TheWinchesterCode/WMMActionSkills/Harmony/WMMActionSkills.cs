﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class WMMActionSkills
{
    public class WMMActionSkills_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(ItemActionEntryScrap))]
    [HarmonyPatch("OnActivated")]
    class PatchItemActionEntryScrapOnActivated
    {
        static bool Prefix(ItemActionEntryScrap __instance, out KeyValuePair<int, int> __state)
        {
            // This patch increases the number of GunParts returned as the quality of the item increases. This helps counter the fact items degrade and GunParts are hard to find. 

            __state = new KeyValuePair<int, int>(0, 0);
            XUiC_ItemStack xuiC_ItemStack = (XUiC_ItemStack)__instance.ItemController;
            ItemStack itemStack = xuiC_ItemStack.ItemStack.Clone();
            Recipe scrapableRecipe = CraftingManager.GetScrapableRecipe(itemStack.itemValue, itemStack.count);
            if (scrapableRecipe == null)
            {
                return true;
            }

            ItemClass forId = ItemClass.GetForId(scrapableRecipe.itemValueType);
            if (!forId.GetItemName().ToLower().EndsWith("parts"))
            {
                return true;
            }

            ItemClass forId2 = ItemClass.GetForId(itemStack.itemValue.type);
            __state = new KeyValuePair<int, int>(itemStack.itemValue.type, forId2.GetWeight());

            var perc = itemStack.itemValue.Quality / 400f;
            var newWeight = (int)(forId2.GetWeight() + (forId2.GetWeight() * perc));

            forId2.SetWeight(newWeight);

            return true;
        }

        static void Postfix(ItemActionEntryScrap __instance, KeyValuePair<int, int> __state)
        {
            if (__state.Key > 0)
            {
                ItemClass forId2 = ItemClass.GetForId(__state.Key);
                forId2.SetWeight(__state.Value);
            }
        }
    }

    [HarmonyPatch(typeof(MinEventActionShowToolbeltMessage))]
    [HarmonyPatch("Execute")]
    [HarmonyPatch(new Type[] { typeof(MinEventParams) })]
    class PatchMinEventActionShowToolbeltMessageExecute
    {
        static bool Prefix(MinEventActionShowToolbeltMessage __instance, ref List<EntityAlive> ___targets, ref string ___message, ref string ___sound)
        {
            for (int i = 0; i < ___targets.Count; i++)
            {
                if (___targets[i] as EntityPlayerLocal != null)
                {
                    var message = ___message;
                    if (message.Contains("@"))
                    {
                        foreach(var split in message.Split(' '))
                        {
                            if (split.StartsWith("@"))
                            {
                                var cvarName = split.Substring(1);
                                if (___targets[i].Buffs.HasCustomVar(cvarName))
                                {
                                    message = message.Replace(split, ___targets[i].Buffs.GetCustomVar(cvarName).ToString());
                                }
                            }
                        }
                    }

                    if (___sound != null)
                    {
                        GameManager.ShowTooltip(___targets[i] as EntityPlayerLocal, message, string.Empty, ___sound, null);
                    }
                    else
                    {
                        GameManager.ShowTooltip(___targets[i] as EntityPlayerLocal, message);
                    }
                }
            }

            return false;
        }
    }
}

