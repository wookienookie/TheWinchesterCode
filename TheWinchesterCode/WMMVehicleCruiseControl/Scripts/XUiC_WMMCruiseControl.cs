﻿public class XUiC_WMMCruiseControl : XUiController
{
	public EntityPlayer LocalPlayer { get; internal set; }

	public EntityVehicle Vehicle { get; internal set; }

	private XUiV_Label textContent;

	private XUiV_Sprite barContent;

	public override void Init()
	{
		base.Init();
		IsDirty = true;
		XUiController childById = GetChildById("BarContent");
		if (childById != null)
		{
			barContent = (XUiV_Sprite)childById.ViewComponent;
		}
		XUiController childById2 = GetChildById("TextContent");
		if (childById2 != null)
		{
			textContent = (XUiV_Label)childById2.ViewComponent;
		}
    }

	public override void Update(float _dt)
	{
		base.Update(_dt);

		if (LocalPlayer == null && XUi.IsGameRunning())
		{
			LocalPlayer = xui.playerUI.entityPlayer;
		}

		if (LocalPlayer != null)
		{
			if (Vehicle == null && LocalPlayer.AttachedToEntity != null && LocalPlayer.AttachedToEntity is EntityVehicle)
			{
				Vehicle = (EntityVehicle)LocalPlayer.AttachedToEntity;
				IsDirty = true;
				xui.CollectedItemList.SetYOffset(100);
			}
			else if (Vehicle != null && LocalPlayer.AttachedToEntity == null)
			{
				Vehicle = null;
				IsDirty = true;
			}
		}

		if (Vehicle != null)
		{
			var _player = Vehicle.GetAttachedPlayerLocal();
			ViewComponent.IsVisible = true;

			switch (_player.GetCVar(WMMVehicleCruiseControl.VehicleAutoGo))
			{
				case 0:
					textContent.Text = "Off";
					barContent.Color = new UnityEngine.Color32(96, 96, 96, 128);
					break;
				case 1:
					textContent.Text = "Slow";
					barContent.Color = new UnityEngine.Color32(209, 191, 31, 128);
					break;
				case 2:
					textContent.Text = "Sprint";
					barContent.Color = new UnityEngine.Color32(42, 209, 84, 128);
					break;
			}
		}
        else
        {
			ViewComponent.IsVisible = false;
        }
    }
}