﻿using HarmonyLib;
using System;
using System.Reflection;
using UnityEngine;

public class WMMVehicleCruiseControl
{
    public const string VehicleAutoGo = "vehicleautogo";
    private static ulong AutoRunMarkedTime = 0;

    public class WMMVehicleCruiseControl_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(EntityVehicle))]
    [HarmonyPatch("MoveByAttachedEntity")]
    [HarmonyPatch(new Type[] { typeof(EntityPlayerLocal) })]
    public class PatchEntityVehicleMoveByAttachedEntity
    {
        static void Postfix(EntityVehicle __instance, EntityPlayerLocal _player, MovementInput ___movementInput)
        {
            if (Input.GetKey(KeyCode.Q) && (AutoRunMarkedTime == 0 || GameTimer.Instance.ticks > AutoRunMarkedTime))
            {
                switch (_player.GetCVar(VehicleAutoGo))
                {
                    case 0:
                        _player.SetCVar(VehicleAutoGo, 1);
                        break;
                    case 1:
                        _player.SetCVar(VehicleAutoGo, 2);
                        break;
                    case 2:
                        _player.SetCVar(VehicleAutoGo, 0);
                        break;
                }

                AutoRunMarkedTime = GameTimer.Instance.ticks + 5;
            }

            LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(_player);
            if (uiforPlayer == null || uiforPlayer.playerInput == null)
            {
                return;
            }

            PlayerActionsVehicle vehicleActions = uiforPlayer.playerInput.VehicleActions;

            if (vehicleActions.Move.Y > 0 ||
                vehicleActions.MoveBack.IsPressed ||
                (vehicleActions.Brake.IsPressed && __instance as EntityVHelicopter == null && __instance as EntityVGyroCopter == null))
            {
                _player.SetCVar(VehicleAutoGo, 0);
            }

            if (_player.GetCVar(VehicleAutoGo) == 1)
            {
                ___movementInput.moveForward = 1;
                ___movementInput.running = false;
            }
            else if (_player.GetCVar(VehicleAutoGo) == 2)
            {
                ___movementInput.moveForward = 1;
                ___movementInput.running = true;
            }
        }
    }

    [HarmonyPatch(typeof(EntityVehicle))]
    [HarmonyPatch("DetachEntity")]
    [HarmonyPatch(new Type[] { typeof(Entity) })]
    public class PatchEntityVehicleDetachEntity
    {
        static void Postfix(EntityVehicle __instance, Entity _entity)
        {
            if (!_entity.isEntityRemote && GameManager.Instance.World != null)
            {
                (_entity as EntityPlayerLocal).SetCVar(VehicleAutoGo, 0);
                AutoRunMarkedTime = 0;
            }
        }
    }

    [HarmonyPatch(typeof(EntityVehicle))]
    [HarmonyPatch("AttachEntityToSelf")]
    [HarmonyPatch(new Type[] { typeof(Entity), typeof(int) })]
    public class PatchEntityVehicleAttachEntityToSelf
    {
        static void Postfix(EntityVehicle __instance, Entity _entity)
        {
            if (!_entity.isEntityRemote && GameManager.Instance.World != null)
            {
                (_entity as EntityPlayerLocal).SetCVar(VehicleAutoGo, 0);
                AutoRunMarkedTime = 0;
            }
        }
    }
}

