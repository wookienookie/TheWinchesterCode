﻿using System;
using HarmonyLib;
using UnityEngine;
using System.Reflection;

public class WMMVideoOptionShowOnScreenSettings
{
    public class WMMVideoOptionShowOnScreenSettings_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_OnScreenIcons.OnScreenIcon))]
    [HarmonyPatch("Update")]
    [HarmonyPatch(new Type[] { typeof(Vector3), typeof(Vector3), typeof(Vector3), typeof(XUi), typeof(int) }, new ArgumentType[] { ArgumentType.Normal, ArgumentType.Normal, ArgumentType.Normal, ArgumentType.Normal, ArgumentType.Ref })]
    public class PatchXUiC_OnScreenIconsOnScreenIconUpdate
    {
        static bool Prefix(XUiC_OnScreenIcons.OnScreenIcon __instance)
        {
            if (!WMMVideoOptions.VideoOptions.ShowOnScreenSprites)
            {
                __instance.Transform.localPosition = NavObject.InvalidPos;
                return false;
            }

            return true;
        }
    }
}
