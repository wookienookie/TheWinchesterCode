﻿using System;
using HarmonyLib;
using UnityEngine;
using System.Reflection;

public class WMMVideoOptionHideXPPopup
{
    public class WMMVideoOptionHideXPPopup_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_CollectedItemList))]
    [HarmonyPatch("AddIconNotification")]
    [HarmonyPatch(new Type[] { typeof(string), typeof(int), typeof(bool) })]
    public class PatchXUiC_CollectedItemListAddIconNotification
    {
        //NOTE : Requires XP icon to be called ui_game_symbol_xp

        static bool Prefix(XUiC_CollectedItemList __instance, ref string iconNotifier, ref int count, ref bool _bAddOnlyIfNotExisting)
        {
            if(WMMVideoOptions.VideoOptions.ShowXPPopUp)
            {
                return true;
            }

            if(iconNotifier == "ui_game_symbol_xp")
                return false;

            return true;
        }
    }
}
