﻿using System;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using UnityEngine;
using System.IO;

public class WMMVideoOptions
{
    private static bool ConfigsLoaded = false;
    private static string VideoOptionsFilename = "WMMVideoOptions.xml";
    private static string SaveDataPath = "";
    public static bool ReloadUI = false;

    public static void LoadWMMVideoOptions()
    {
        var mod = ModManager.GetMod("WMMVideoOptions", true);
        SaveDataPath = mod.Path + "/SaveData";
        Directory.CreateDirectory(SaveDataPath);
        Log.Out("WMMVideoOptions SaveDataPath = " + SaveDataPath);
        LoadConfigs();
    }

    public static void LoadConfigs()
    {
        if (!ConfigsLoaded)
        {
            ConfigsLoaded = true;
            LoadVideoOptions();
            Log.Out("WMM Video Options Loaded.");
        }
    }

    private static void LoadVideoOptions()
    {
        var path = SaveDataPath + "/" + VideoOptionsFilename;
        if (File.Exists(path)) 
        {
            XmlFile xmlFile = new XmlFile(SaveDataPath, VideoOptionsFilename);
            XElement root = xmlFile.XmlDoc.Root;
            if (root == null || !root.HasElements)
            {
                throw new Exception("No element <wmm_videooptions> found!");
            }

            foreach (XElement xelement in root.Elements())
            {
                if (xelement.Name.LocalName.Equals("config"))
                {
                    if (!xelement.HasAttribute("name"))
                    {
                        throw new Exception("Attribute 'name' missing on item");
                    }

                    if (!xelement.HasAttribute("value"))
                    {
                        throw new Exception("Attribute 'value' missing on item");
                    }

                    var value = xelement.GetAttribute("value");
                    switch (xelement.GetAttribute("name"))
                    {
                        case "ShowCrosshair":
                            if (bool.TryParse(value, out bool showCrosshair))
                                VideoOptions.ShowCrosshair = showCrosshair;
                            break;
                        case "CrosshairColor":
                            VideoOptions.CrosshairColor = GetColor(value);
                            break;
                        case "ShowActivationText":
                            if (bool.TryParse(value, out bool showActivationText))
                                VideoOptions.ShowActivationText = showActivationText;
                            break;
                        case "ShowOnScreenSprites":
                            if (bool.TryParse(value, out bool showOnScreenSprites))
                                VideoOptions.ShowOnScreenSprites = showOnScreenSprites;
                            break;
                        case "ShowZombieHealthBar":
                            if (bool.TryParse(value, out bool showZombieHealthBar))
                                VideoOptions.ShowZombieHealthBar = showZombieHealthBar;
                            break;
                        case "ShowXPPopUp":
                            if (bool.TryParse(value, out bool showXPPopUp))
                                VideoOptions.ShowXPPopUp = showXPPopUp;
                            break;
                    }
                }
            }
        }
        else
        {
            // This creates a new wmm_videooptions.xml with default values
            SaveVideoOptions();
        }
    }

    private static XmlElement CreateXMLGameOptionChild(XmlDocument xmlDocument, string name, string value)
    {
        XmlElement xmlElement = xmlDocument.CreateElement("config");
        XmlAttribute xmlAttributeName = xmlDocument.CreateAttribute("name");
        xmlAttributeName.Value = name;
        xmlElement.Attributes.Append(xmlAttributeName);
        XmlAttribute xmlAttributeValue = xmlDocument.CreateAttribute("value");
        xmlAttributeValue.Value = value.ToLower();
        xmlElement.Attributes.Append(xmlAttributeValue);

        return xmlElement;
    }

    private static string SerializeToString(XmlDocument xmlDocument)
    {
        string result;
        using (StringWriter stringWriter = new StringWriter())
        {
            using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter))
            {
                xmlDocument.WriteTo(xmlTextWriter);
                result = stringWriter.ToString();
            }
        }
        return result;
    }

    public static void SaveVideoOptions()
    {
        XmlDocument xmlDocument = new XmlDocument();
        XmlDeclaration newChild = xmlDocument.CreateXmlDeclaration("1.0", "UTF-8", null);
        xmlDocument.InsertBefore(newChild, xmlDocument.DocumentElement);
        XmlNode parent = xmlDocument.AppendChild(xmlDocument.CreateElement("wmm_videooptions"));

        parent.AppendChild(CreateXMLGameOptionChild(xmlDocument, "ShowCrosshair", VideoOptions.ShowCrosshair.ToString()));
        parent.AppendChild(CreateXMLGameOptionChild(xmlDocument, "CrosshairColor", ColorUtility.ToHtmlStringRGBA(VideoOptions.CrosshairColor)));
        parent.AppendChild(CreateXMLGameOptionChild(xmlDocument, "ShowActivationText", VideoOptions.ShowActivationText.ToString()));
        parent.AppendChild(CreateXMLGameOptionChild(xmlDocument, "ShowOnScreenSprites", VideoOptions.ShowOnScreenSprites.ToString()));
        parent.AppendChild(CreateXMLGameOptionChild(xmlDocument, "ShowZombieHealthBar", VideoOptions.ShowZombieHealthBar.ToString()));
        parent.AppendChild(CreateXMLGameOptionChild(xmlDocument, "ShowXPPopUp", VideoOptions.ShowXPPopUp.ToString()));

        Log.Out(SerializeToString(xmlDocument));


        // Make sure folder is there before saving
        Directory.CreateDirectory(SaveDataPath);
        var path = SaveDataPath + "/" + VideoOptionsFilename;
        File.WriteAllText(path, SerializeToString(xmlDocument), Encoding.UTF8);
    }

    private static Color GetColor(string color)
    {
        // https://forum.unity.com/threads/colorutility-tryparsehtmlstring-in-unity3d-5-2-not-working.353523/ Stupid bug

        // Order to check values
        // f11212ff - need to append # to convert
        // white
        // 128,34,134,255

        var returnColor = VideoOptions.CrosshairColorDefault;
        if (!ColorUtility.TryParseHtmlString(string.Format("#{0}", color), out returnColor))
        {
            if (!ColorUtility.TryParseHtmlString(color, out returnColor))
            {
                if (color.Contains(','))
                {
                    var colors = color.Split(',');

                    for (var i = 0; i < colors.Length; i++)
                    {
                        var parsedColorInt = 0;
                        if (int.TryParse(colors[i], out parsedColorInt))
                        {
                            returnColor[i] = float.Parse(colors[i]);
                        }
                        else
                            return VideoOptions.CrosshairColorDefault;
                    }
                }
            }
        }

        return returnColor;
    }

    public static class VideoOptions
    {
        static VideoOptions()
        {
            LoadWMMVideoOptions();
        }

        public const bool ShowCrosshairDefault = true;
        public static Color CrosshairColorDefault = Color.white;
        public const bool ShowActivationTextDefault = true;
        public const bool ShowOnScreenSpritesDefault = false;
        public const bool ShowZombieHealthBarDefault = false;
        public const bool ShowXPPopUpDefault = false;

        public static bool ShowCrosshair = ShowCrosshairDefault;
        public static Color CrosshairColor = CrosshairColorDefault;
        public static bool ShowActivationText = ShowActivationTextDefault;
        public static bool ShowOnScreenSprites = ShowOnScreenSpritesDefault;
        public static bool ShowZombieHealthBar = ShowZombieHealthBarDefault;
        public static bool ShowXPPopUp = ShowXPPopUpDefault;
    }
}

