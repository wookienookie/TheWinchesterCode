﻿using HarmonyLib;

public class XUiC_WMMOptionsVideo : XUiC_OptionsVideo
{
    public XUiC_ComboBoxBool comboShowCrosshair;
    public XUiC_ColorPicker colorPickerCrosshairColor;
    public XUiC_ComboBoxBool comboShowActivationText;
    public XUiC_ComboBoxBool comboShowOnScreenSprites;
    public XUiC_ComboBoxBool comboShowZombieHealthBar;
    public XUiC_ComboBoxBool comboShowXPPopUp;

    private bool Reload = false;

    public override void Init()
    {
        base.Init();

        comboShowCrosshair = GetChildById("ShowCrosshair").GetChildByType<XUiC_ComboBoxBool>();
        colorPickerCrosshairColor = GetChildById("CrosshairColor").GetChildByType<XUiC_ColorPicker>();
        comboShowActivationText = GetChildById("ShowActivationText").GetChildByType<XUiC_ComboBoxBool>();
        comboShowOnScreenSprites = GetChildById("ShowOnScreenSprites").GetChildByType<XUiC_ComboBoxBool>();
        comboShowZombieHealthBar = GetChildById("ShowZombieHealthBar").GetChildByType<XUiC_ComboBoxBool>();
        comboShowXPPopUp = GetChildById("ShowXPPopUp").GetChildByType<XUiC_ComboBoxBool>();

        comboShowCrosshair.OnValueChanged += Combo_OnValueChanged;
        colorPickerCrosshairColor.OnSelectedColorChanged += ColorPicker_OnSelectedColorChanged;
        comboShowActivationText.OnValueChanged += Combo_OnValueChanged;
        comboShowOnScreenSprites.OnValueChanged += Combo_OnValueChanged;
        comboShowZombieHealthBar.OnValueChanged += Combo_OnValueChanged;
        comboShowXPPopUp.OnValueChanged += Combo_OnValueChanged;

        var btnApply = AccessTools.Field(typeof(XUiC_OptionsVideo), "btnApply").GetValue(this) as XUiC_SimpleButton;
        btnApply.OnPressed += BtnApply_OnPressed;

        var btnBack = AccessTools.Field(typeof(XUiC_OptionsVideo), "btnBack").GetValue(this) as XUiC_SimpleButton;
        btnBack.OnPressed += BtnBack_OnPressed;
    }

    private void BtnBack_OnPressed(XUiController _sender, int _mouseButton)
    {
        // Need these local variables as we need to reload the UI when the user press the Back button. If we did it on the Apply button the UI can crash.
        if (Reload)
        {
            WMMVideoOptions.ReloadUI = true;
            Reload = false;
        }
    }

    public override void OnOpen()
    {
        base.OnOpen();
        LoadOptions();
    }

    private void BtnApply_OnPressed(XUiController _sender, int _mouseButton)
    {
        WMMVideoOptions.VideoOptions.CrosshairColor = colorPickerCrosshairColor.SelectedColor;
        WMMVideoOptions.VideoOptions.ShowCrosshair = comboShowCrosshair.Value;
        WMMVideoOptions.VideoOptions.ShowActivationText = comboShowActivationText.Value;
        WMMVideoOptions.VideoOptions.ShowOnScreenSprites = comboShowOnScreenSprites.Value;
        WMMVideoOptions.VideoOptions.ShowZombieHealthBar = comboShowZombieHealthBar.Value;
        WMMVideoOptions.VideoOptions.ShowXPPopUp = comboShowXPPopUp.Value;

        WMMVideoOptions.SaveVideoOptions();
    }

    private void ColorPicker_OnSelectedColorChanged(UnityEngine.Color color)
    {
        EnableApplyButton();
    }

    private void Combo_OnValueChanged(XUiController _sender, bool _oldValue, bool _newValue)
    {
        EnableApplyButton();
    }

    private void EnableApplyButton()
    {
        var btnApply = AccessTools.Field(typeof(XUiC_OptionsVideo), "btnApply").GetValue(this) as XUiC_SimpleButton;
        btnApply.Enabled = true;
        Reload = true;
    }

    private void LoadOptions()
    {
        colorPickerCrosshairColor.SelectedColor = WMMVideoOptions.VideoOptions.CrosshairColor;
        comboShowCrosshair.Value = WMMVideoOptions.VideoOptions.ShowCrosshair;
        comboShowActivationText.Value = WMMVideoOptions.VideoOptions.ShowActivationText;
        comboShowOnScreenSprites.Value = WMMVideoOptions.VideoOptions.ShowOnScreenSprites;
        comboShowZombieHealthBar.Value = WMMVideoOptions.VideoOptions.ShowZombieHealthBar;
        comboShowXPPopUp.Value = WMMVideoOptions.VideoOptions.ShowXPPopUp;
    }
}