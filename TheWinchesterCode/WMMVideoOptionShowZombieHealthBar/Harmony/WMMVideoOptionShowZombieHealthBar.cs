﻿using System;
using HarmonyLib;
using UnityEngine;
using System.Reflection;

public class WMMVideoOptionShowZombieHealthBar
{
    public class WMMVideoOptionShowZombieHealthBar_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_TargetBar))]
    [HarmonyPatch("Update")]
    [HarmonyPatch(new Type[] { typeof(float) })]
    public class PatchXUiC_TargetBarUpdate
    {
        static bool Prefix(XUiC_TargetBar __instance)
        {
            if (!WMMCustomVideoOptions.VideoOptions.ShowZombieHealthBar)
            {
                __instance.ViewComponent.IsVisible = false;
                return false;
            }

            return true;
        }
    }
}
