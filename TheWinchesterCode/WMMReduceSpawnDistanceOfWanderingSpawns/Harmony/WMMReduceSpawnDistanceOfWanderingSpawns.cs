﻿using System;
using HarmonyLib;
using System.Reflection;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Linq;
using UnityEngine;

public class WMMReduceSpawnDistanceOfWanderingSpawns
{
    public class WMMReduceSpawnDistanceOfWanderingSpawns_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(AIDirectorHordeComponent))]
    [HarmonyPatch("FindTargets")]
    [HarmonyPatch(new Type[] { typeof(Vector3), typeof(Vector3), typeof(Vector3), typeof(List<AIDirectorPlayerState>) }, new ArgumentType[] { ArgumentType.Out, ArgumentType.Out, ArgumentType.Out, ArgumentType.Normal })]
    static class PatchAIDirectorHordeComponentFindTargets
    {
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
            var codes = new List<CodeInstruction>(instructions);

            for (int i = 0; i < codes.Count; i++)
            {
                if (codes[i].opcode == OpCodes.Ldc_R4 && (float)codes[i].operand == 92f)
                {
                    codes[i].operand = 70f;
                }
            }
            return codes.AsEnumerable();
        }
    }
}
