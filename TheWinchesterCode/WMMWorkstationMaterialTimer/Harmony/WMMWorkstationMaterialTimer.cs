﻿using HarmonyLib;
using System.Reflection;
using UnityEngine;
using System;

public class WMMWorkstationMaterialTimer
{
    public class WMMWorkstationMaterialTimer_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_WorkstationMaterialInputGrid))]
    [HarmonyPatch("Update")]
    [HarmonyPatch(new Type[] { typeof(float) })]
    public class PatchXUiC_WorkstationMaterialInputGridUpdate
    {
        static bool Prefix(XUiC_WorkstationMaterialInputGrid __instance, ref float _dt, ref XUiM_Workstation ___workstationData, ref XUiController[] ___itemControllers)
        {
            // Call base.Update() in XUiController class
            PatchXUiController.Update(__instance, _dt);

            int num = 0;
            while (num < ___workstationData.TileEntity.Input.Length && num < ___itemControllers.Length)
            {
                float timerForSlot = ___workstationData.TileEntity.GetTimerForSlot(num);
                if (timerForSlot > 0f)
                {
                    var adjustedTime = GetTotalTime(___workstationData.TileEntity, num) + timerForSlot + 0.95f;
                    ((XUiC_ItemStack)___itemControllers[num]).timer.IsVisible = true;
                    ((XUiC_ItemStack)___itemControllers[num]).timer.Text = string.Format("{0}:{1}:{2}", Mathf.Floor((adjustedTime) / 3600f).ToCultureInvariantString("00"), Mathf.Floor((adjustedTime%3600f) / 60f).ToCultureInvariantString("00"), Mathf.Floor((adjustedTime) % 60f).ToCultureInvariantString("00"));
                }
                else
                {
                    ((XUiC_ItemStack)___itemControllers[num]).timer.IsVisible = false;
                }
                num++;
            }
            ___workstationData.GetIsBurning();

            return false;
        }

        private static float GetTotalTime(TileEntityWorkstation tileEntity, int slotId)
        {
            var materialNames = AccessTools.Field(typeof(TileEntityWorkstation), "materialNames").GetValue(tileEntity) as string[];
            var input = AccessTools.Field(typeof(TileEntityWorkstation), "input").GetValue(tileEntity) as ItemStack[];
            var isModuleUsed = AccessTools.Field(typeof(TileEntityWorkstation), "isModuleUsed").GetValue(tileEntity) as bool[];
            var tools = AccessTools.Field(typeof(TileEntityWorkstation), "tools").GetValue(tileEntity) as ItemStack[];

            ItemClass forId = ItemClass.GetForId(input[slotId].itemValue.type);
            float time = 0f;

            if (forId != null)
            {
                for (int j = 0; j < materialNames.Length; j++)
                {
                    if (forId.MadeOfMaterial.ForgeCategory != null && forId.MadeOfMaterial.ForgeCategory.EqualsCaseInsensitive(materialNames[j]))
                    {
                        ItemClass itemClass = ItemClass.GetItemClass("unit_" + materialNames[j], false);
                        if (itemClass != null && itemClass.MadeOfMaterial.ForgeCategory != null)
                        {
                            time = (float)forId.GetWeight() * ((forId.MeltTimePerUnit > 0f) ? forId.MeltTimePerUnit : 1f);
                            if (isModuleUsed[0])
                            {
                                for (int k = 0; k < tools.Length; k++)
                                {
                                    float num2 = 1f;
                                    tools[k].itemValue.ModifyValue(null, null, PassiveEffects.CraftingSmeltTime, ref time, ref num2, FastTags.Parse(forId.Name), true);
                                    time *= num2;
                                }
                            }
                        }
                    }
                }
            }

            if(time > 0f && input[slotId].count >= 1)
            {
                return time * (input[slotId].count - 1);
            }

            return time;
        }
    }

    [HarmonyPatch(typeof(XUiC_WorkstationWindowGroup))]
    [HarmonyPatch("Update")]
    [HarmonyPatch(new Type[] { typeof(float) })]
    public class PatchXUiC_WorkstationWindowGroup
    {
        static void Postfix(XUiC_WorkstationWindowGroup __instance, ref float _dt, ref XUiC_WorkstationFuelGrid ___fuelWindow, ref XUiC_CraftingQueue ___craftingQueue, ref XUiV_Label ___burnTimeLeft)
        {
            if (___fuelWindow != null && ___craftingQueue != null && ___burnTimeLeft != null && __instance.WorkstationData != null)
            {
                float totalBurnTimeLeft = __instance.WorkstationData.GetTotalBurnTimeLeft();
                ___burnTimeLeft.Text = string.Format("{0}:{1}:{2}", Mathf.Floor((totalBurnTimeLeft) / 3600f).ToCultureInvariantString("00"), Mathf.Floor((totalBurnTimeLeft % 3600f) / 60f).ToCultureInvariantString("00"), Mathf.Floor((totalBurnTimeLeft) % 60f).ToCultureInvariantString("00"));
            }
        }
    }

    [HarmonyPatch]
    public class PatchXUiController
    {
        [HarmonyReversePatch]
        [HarmonyPatch(typeof(XUiController), "Update")]
        public static void Update(object instance, float _dt)
        {
            // Do nothing, this is used to call base method XUiController.Update()
        }
    }
}
