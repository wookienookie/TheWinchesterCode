﻿using System;
using HarmonyLib;
using System.Reflection;
using UnityEngine;

public class WMMVideoOptionShowActivationText
{
    public class WMMVideoOptionShowActivationText_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_InteractionPrompt))]
    [HarmonyPatch("Update")]
    [HarmonyPatch(new Type[] { typeof(float) })]
    class PatchXUiC_InteractionPromptUpdate
    {
        static void Postfix(XUiC_InteractionPrompt __instance, float _dt, ref string ___text)
        {
            if (!WMMCustomVideoOptions.VideoOptions.ShowActivationText)
            {
                ___text = "";
                __instance.xui.playerUI.windowManager.Close(XUiC_InteractionPrompt.ID);
            }
        }
    }
    
}
