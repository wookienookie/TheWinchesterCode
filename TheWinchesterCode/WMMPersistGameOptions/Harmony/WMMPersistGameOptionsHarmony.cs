﻿using System;
using HarmonyLib;
using System.Reflection;
using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class WMMPersistGameOptionsHarmony_Init : IModApi
{
    public void InitMod(Mod _modInstance)
    {
        Log.Out(" Loading Patch: " + this.GetType().ToString());

        // Reduce extra logging stuff
        Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
        Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

        var harmony = new HarmonyLib.Harmony(GetType().ToString());
        harmony.PatchAll(Assembly.GetExecutingAssembly());
    }
}

[HarmonyPatch(typeof(XUiC_NewContinueGame))]
[HarmonyPatch("updateGameOptionValues")]
[HarmonyPatch(new Type[] { })]
public class Patch_XUiC_NewContinueGame_updateGameOptionValues
{
    static bool Prefix(XUiC_GamePrefSelector __instance, bool ___isContinueGame)
    {
        // If continue game dont load default values!
        if (___isContinueGame)
        {
            return true;
        }

        var mod = ModManager.GetMod("WMMPersistGameOptions", true);
        var SaveDataPath = mod.Path + "/SaveData";
        Directory.CreateDirectory(SaveDataPath);

        Log.Out("Load Persist Game Options : " + SaveDataPath);

        GamePrefs.Instance.Load(SaveDataPath + "/gameOptions.sdf");

        return true;
    }
}

[HarmonyPatch(typeof(XUiC_NewContinueGame))]
[HarmonyPatch("SaveGameOptions")]
[HarmonyPatch(new Type[] { })]
public class Patch_XUiC_NewContinueGame_SaveGameOptions
{
    static void Postfix(XUiC_NewContinueGame __instance, Dictionary<EnumGamePrefs, XUiC_GamePrefSelector> ___gameOptions)
    {
        var mod = ModManager.GetMod("WMMPersistGameOptions", true);
        var SaveDataPath = mod.Path + "/SaveData";
        Directory.CreateDirectory(SaveDataPath);

        Log.Out("Save Persist Game Options  : " + SaveDataPath);

        List<EnumGamePrefs> list = new List<EnumGamePrefs>(___gameOptions.Count);
        ___gameOptions.CopyKeysTo(list);
        GamePrefs.Instance.Save(SaveDataPath + "/gameOptions.sdf", list);
    }
}
