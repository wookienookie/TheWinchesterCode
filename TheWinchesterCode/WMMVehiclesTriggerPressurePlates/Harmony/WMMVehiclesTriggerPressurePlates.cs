﻿using HarmonyLib;
using System.Reflection;
using UnityEngine;

public class WMMVehiclesTriggerPressurePlates
{
    public class WMMVehiclesTriggerPressurePlates_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(EntityVehicle))]
    [HarmonyPatch("PhysicsFixedUpdate")]
    public class PatchEntityVehiclePhysicsFixedUpdate
    {
        static void Postfix(EntityVehicle __instance)
        {
            var entityPlayer = __instance.GetAttachedPlayerLocal();
            if (entityPlayer != null)
            {
                __instance.world.CheckEntityCollisionWithBlocks(entityPlayer);
            }
        }
    }
}
