﻿using System;
using HarmonyLib;
using System.Reflection;
using UnityEngine;

public class WMMTrackRecipe_Init : IModApi
{
    public void InitMod(Mod _modInstance)
    {
        Log.Out(" Loading Patch: " + this.GetType().ToString());

        // Reduce extra logging stuff
        Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
        Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

        var harmony = new HarmonyLib.Harmony(GetType().ToString());
        harmony.PatchAll(Assembly.GetExecutingAssembly());
    }
}

[HarmonyPatch(typeof(XUiC_RecipeTrackerWindow))]
[HarmonyPatch("GetBindingValue")]
[HarmonyPatch(new Type[] { typeof(string), typeof(string) }, new ArgumentType[] { ArgumentType.Ref, ArgumentType.Normal })]
public class Patch_XUiC_RecipeTrackerWindow_GetBindingValue
{
    static bool Prefix(XUiC_RecipeTrackerWindow __instance, ref bool __result, ref string value, ref string bindingName)
    {
        if (bindingName == "showrecipe")
        {
            value = true.ToString();
            __result = true;
            return false;
        }

        return true;
    }
}

[HarmonyPatch(typeof(XUiC_QuestTrackerWindow))]
[HarmonyPatch("GetBindingValue")]
[HarmonyPatch(new Type[] { typeof(string), typeof(string) }, new ArgumentType[] { ArgumentType.Ref, ArgumentType.Normal })]
public class Patch_XUiC_QuestTrackerWindow_GetBindingValue
{
    static bool Prefix(XUiC_RecipeTrackerWindow __instance, ref bool __result, ref string value, ref string bindingName)
    {
        if (bindingName == "showquest")
        {
            value = true.ToString();
            __result = true;
            return false;
        }

        return true;
    }
}

//[HarmonyPatch(typeof(XUiM_Recipes), "TrackedRecipe", MethodType.Setter)]
//public class Patch_NavObject_HasRequirements
//{
//    static bool Prefix(NavObject __instance, bool __result)
//    {
//        // See XUiC_WMMMapArea.ToggleNavObjectVisibility() how this horrible hack works!
//        if (__instance.HiddenDisplayName == WMMConstants.HiddenDisplayName
//            && !__instance.HasOnScreen)
//        {
//            __result = false;
//            return false;
//        }

//        return true;
//    }
//}


//[HarmonyPatch("GameInstance", MethodType.Setter)]
//static class GameInstance_Setter_Patch
//{
//    static void Postfix(ref GameInstance __instance, int newValue)
//    {
//        // Change the value of the property before it is set
//        newValue = 10;
//    }
//}