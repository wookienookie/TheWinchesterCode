﻿using System;
using HarmonyLib;
using System.Reflection;
using UnityEngine;

public class WMMDisableVehiclesDuringBloodmoon
{
    public class WMMDisableVehiclesDuringBloodmoon_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(VPEngine))]
    [HarmonyPatch("Update")]
    [HarmonyPatch(new Type[] { typeof(float) })]
    public class PatchVPEngineUpdate
    {
        static bool Prefix(VPEngine __instance, float _dt)
        {
            var num = (int)SkyManager.dayCount;
            var bloodMoonDay = GameStats.GetInt(EnumGameStats.BloodMoonDay);
            var isBloodMoon = (num == bloodMoonDay && SkyManager.TimeOfDay() >= 22f) || (num > 1 && num == bloodMoonDay + 1 && SkyManager.TimeOfDay() <= 6f);

            if (isBloodMoon)
            {
                MethodInfo method = AccessTools.Method(typeof(VPEngine), "stopEngine");
                method.Invoke(__instance, new object[] { false });

                return false;
            }

            return true;
        }
    }
}