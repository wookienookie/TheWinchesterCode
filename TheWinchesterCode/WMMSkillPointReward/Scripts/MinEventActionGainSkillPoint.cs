﻿public class MinEventActionGainSkillPoint : MinEventActionTargetedBase
{
    public override void Execute(MinEventParams _params)
    {
        for (var i = 0; i < targets.Count; i++)
        {
            var entity = targets[i] as EntityPlayer;
            if (entity != null)
                entity.Progression.SkillPoints += 1;
        }
    }
}
