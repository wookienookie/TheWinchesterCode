﻿using System;
using HarmonyLib;
using System.Reflection;
using UnityEngine;

public class WMMRemoveTraderXP
{
    public class WMMRemoveTraderXP_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(Progression))]
    [HarmonyPatch("AddLevelExp")]
    [HarmonyPatch(new Type[] { typeof(int), typeof(string), typeof(Progression.XPTypes), typeof(bool) })]
    class PatchProgressionAddLevelExp
    {
        static bool Prefix(Progression __instance, ref int _exp, ref string _cvarXPName, ref Progression.XPTypes _xpType, ref bool useBonus)
        {
            if (_cvarXPName == "_xpFromSelling")
                return false;

            return true;
        }
    }
}
