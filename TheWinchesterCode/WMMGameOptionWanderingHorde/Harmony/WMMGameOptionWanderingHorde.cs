﻿//using HarmonyLib;
//using System.Collections.Generic;
//using System.Reflection;
//using UnityEngine;
//using System.Reflection.Emit;
//using System.Linq;

//public class WMMGameOptionWanderingHorde
//{
//    public class WMMGameOptionWanderingHorde_Init : IModApi
//    {
//        public void InitMod(Mod _modInstance)
//        {
//            Log.Out(" Loading Patch: " + this.GetType().ToString());

//            // Reduce extra logging stuff
//            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
//            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

//            var harmony = new HarmonyLib.Harmony(GetType().ToString());
//            harmony.PatchAll(Assembly.GetExecutingAssembly());
//        }
//    }

//    [HarmonyPatch(typeof(AIDirectorWanderingHordeComponent))]
//    [HarmonyPatch("ChooseNextTime")]
//    public class PatchAIDirectorWanderingHordeComponentChooseNextTime
//    {
//        static bool Prefix(AIDirectorWanderingHordeComponent __instance, AIWanderingHordeSpawner.SpawnType _spawnType, ref ulong ___HordeNextTime)
//        {
//            ulong frequency = 24000u;

//            switch (WMMCustomGameOptions.GetInt("WanderingHordeFrequency"))
//            {
//                case 0:
//                    frequency = (ulong)__instance.Random.RandomRange(12000, 24000);
//                    break;
//                case 1:
//                    frequency = (ulong)__instance.Random.RandomRange(10000, 22000);
//                    break;
//                case 2:
//                    frequency = (ulong)__instance.Random.RandomRange(8000, 20000);
//                    break;
//                case 3:
//                    frequency = (ulong)__instance.Random.RandomRange(6000, 18000);
//                    break;
//                case 4:
//                    frequency = (ulong)__instance.Random.RandomRange(4000, 16000);
//                    break;
//                case 5:
//                    frequency = (ulong)__instance.Random.RandomRange(2000, 14000);
//                    break;
//            }

//            ___HordeNextTime = __instance.Director.World.worldTime + frequency; // CHANGED 12000, 24000

//            //Log.Out("ChooseNextWanderingHordeTime m_nextWanderingHordeTime : " + __instance.m_nextWanderingHordeTime);

//            return false;
//        }
//    }

//    [HarmonyPatch(typeof(AIDirectorHordeComponent))]
//    [HarmonyPatch("FindTargets")]
//    public class PatchAIDirectorHordeComponentFindTargets
//    {
//        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
//        {
//            var codes = new List<CodeInstruction>(instructions);

//            for (int i = 0; i < codes.Count; i++)
//            {
//                if (codes[i].opcode == OpCodes.Ldc_I4_S && (sbyte)codes[i].operand == 12)
//                {
//                    codes[i].operand = (sbyte)GetDelay(); // TODO : Change this to an in game call to this method, atm GameDifficulty is always 2 as the actual game hasnt loaded yet
//                }
//            }

//            return codes.AsEnumerable();
//        }
//    }

//    [HarmonyPatch(typeof(AIDirectorGameStagePartySpawner))]
//    [HarmonyPatch("SetupGroup")]
//    public class Patch_AIDirectorGameStagePartySpawnerSetupGroup
//    {
//        static void Postfix(AIDirectorGameStagePartySpawner __instance, ref GameStageDefinition.SpawnGroup ___spawnGroup, ref int ___numToSpawn)
//        {
//            if(___spawnGroup != null)
//            {
//                var multiplier = WMMCore.Random.RandomRange(1, WMMCustomGameOptions.GetInt("WanderingHordeMultiplier") + 1);

//                Log.Out("WanderingHordeMultiplier : " + multiplier);

//                ___numToSpawn = EntitySpawner.ModifySpawnCountByGameDifficulty(___spawnGroup.spawnCount) * multiplier;
//            }
//        }
//    }

//    private static uint GetDelay()
//    {
//        return 6u;

//        //switch (GameStats.GetInt(EnumGameStats.GameDifficulty))
//        //{
//        //    case 0:
//        //        return 12u;
//        //    case 1:
//        //        return 10u;
//        //    case 2:
//        //        return 8u;
//        //    case 3:
//        //        return 6u;
//        //    case 4:
//        //        return 4u;
//        //    case 5:
//        //        return 2u;
//        //}

//        //return 12u;
//    }
//}

