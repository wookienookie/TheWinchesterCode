﻿using HarmonyLib;
using System;
using System.Reflection;
using UnityEngine;

public class WMMAdjustTraderPrices
{
    public class WMMAdjustTraderPrices_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiM_Trader))]
    [HarmonyPatch("GetBuyPrice")]
    [HarmonyPatch(new Type[] { typeof(XUi), typeof(ItemValue), typeof(int), typeof(ItemClass), typeof(int) })]
    public class PatchXUiM_TraderGetBuyPrice
    {
        static void Postfix(XUiM_Trader __instance, ref int __result, XUi _xui, ItemValue itemValue, int count, ItemClass itemClass, int index)
        {
            __result = __result * 2;
        }
    }
}

