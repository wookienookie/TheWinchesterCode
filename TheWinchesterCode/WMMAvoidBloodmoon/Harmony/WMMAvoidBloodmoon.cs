﻿using System;
using HarmonyLib;
using System.Reflection;
using UnityEngine;

public class WMMAvoidBloodmoon
{
    public class WMMAvoidBloodmoon_Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + this.GetType().ToString());

            // Reduce extra logging stuff
            Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
            Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(AIDirectorBloodMoonComponent))]
    [HarmonyPatch("AddPlayerToParty")]
    [HarmonyPatch(new Type[] { typeof(EntityPlayer) })]
    class PatchAIDirectorBloodMoonComponentAddPlayerToParty
    {
        static bool Prefix(AIDirectorBloodMoonComponent __instance, ref EntityPlayer _player)
        {
            if (_player.Buffs.HasBuff("buffAvoidBloodmoon"))
            {
                return false;
            }

            return true;
        }
    }
}
